<?php
    
    Route::match(['get','post'],'/','BackController@index');
    
    Route::group(['middleware'=>['school']],function(){
        Route::get('/dashboard','BackController@dashboard');
        Route::get('/logout','BackController@logout');
        Route::resource('class','ClassManagement');
        Route::resource('subject','subjectManagement');
        Route::resource('student','studentManagement');
        Route::resource('teacher','teacherManagement');
        Route::resource('mapping','subjectMaping');
        Route::resource('timetable','timetableManagement');
        Route::resource('notice','NoticeManagement');
        Route::resource('school','SchoolManagement');
        Route::post('/import','studentManagement@importExcel');
        Route::post('/importTeachers','teacherManagement@excelUpload');
        Route::match(['get','post'],'/app-version','AppvesionController@index');
        // Route::get('/delete_appversion','AppvesionController@delete');
        Route::delete('/delete_appversion/{id}', 'AppvesionController@delete');
    });

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('apiauth')->get('/user', function (Request $request) {
    return $request->user();
});

//////////backend api/////////
	Route::post('login', 'Api\LoginController@login');
	Route::post('signup', 'Api\LoginController@signup');
    Route::post('get-school', 'Api\LoginController@getschool');

	// Route::post('useredit', 'Api\LoginController@useredit');
	// Route::post('check_notification', 'Api\ClassController@checkNotification');
	// Route::post('check_above_version', 'Api\AppVersionController@nextVersion');
	// Route::post('add_update_app_version', 'Api\AppVersionController@addUpdateAppVersion');

	Route::group(['middleware' => ['apiauth']], function() {

	Route::get('logout', 'Api\LoginController@logout');
	Route::post('change_password', 'Api\LoginController@changePassword');
	Route::post('/todelete', 'Api\LoginController@toDelete');
	Route::post('get_user_login', 'Api\LoginController@getUserLogin');


	Route::post('get-class-section', 'Api\ClassSectionController@getClassSection');
	Route::post('get-class-listing-page', 'Api\ClassSectionController@getClassSectionListPage');
	Route::post('class-section-form', 'Api\ClassSectionController@classSectionForm');
	Route::post('update_class_section', 'Api\ClassSectionController@updateClassSection');
	Route::post('add_class_section', 'Api\ClassSectionController@addClassSection');


	Route::post('get-school-listing-page', 'Api\SchoolController@getSchoolListPage');
	Route::post('get-school-form', 'Api\SchoolController@getSchoolForm');
	Route::post('schooladdedit', 'Api\SchoolController@SchoolAddEdit');


	Route::post('get-version-listing-page', 'Api\AppVersionController@getVersionListPage');
	Route::post('app-version-form', 'Api\AppVersionController@appVersionForm');
	Route::post('add_update_app_version', 'Api\AppVersionController@addUpdateAppVersion');


	Route::post('get-subject-listing-page', 'Api\SubjectMasterController@getListPage');
	Route::post('subject-master-form', 'Api\SubjectMasterController@getForm');
	Route::post('update_subject_master', 'Api\SubjectMasterController@updateSubject');

	Route::post('get-class-subject-listing-page', 'Api\SubjectClassController@getListPage');
	Route::post('class-subject-form', 'Api\SubjectClassController@subjectClassForm');
	Route::post('add_update_subject_class', 'Api\SubjectClassController@addUpdateSubjectClass');

	Route::post('get-student-listing-page', 'Api\StudentController@getListPage');
	Route::post('student-form', 'Api\StudentController@getForm');
	Route::post('add_update_student', 'Api\StudentController@addupdateStudents');


	Route::post('get-teacher-listing-page', 'Api\TeacherController@getListPage');
	Route::post('teacher-form', 'Api\TeacherController@getForm');
	Route::post('add_update_teacher', 'Api\TeacherController@addupdateTeachers');
	
	// Route::post('add_update_subject', 'Api\SubjectMasterController@addUpdate');
	// Route::post('get-subject', 'Api\SubjectMasterController@getSubjectList');
	// Route::post('add_subject_master', 'Api\SubjectMasterController@addSubject');
	Route::post('get-timetable-listing-page', 'Api\TimeTableController@getListPage');
	Route::post('timetable-form', 'Api\TimeTableController@timeTableForm');
	Route::post('add_update_timetable', 'Api\TimeTableController@addupdateTimetable');


	Route::post('get-notice-listing-page', 'Api\NoticeController@getListPage');
	Route::post('notice-form', 'Api\NoticeController@noticeForm');
	Route::post('add_update_notice', 'Api\NoticeController@addupdatenotice');

////////////////////////////////////////////////

/////////////Front end api//////////////////////////////
	Route::post('get-notice', 'Api\NoticeController@noticeList');
	Route::post('useredit', 'Api\LoginController@useredit');
	Route::get('get_notification_list', 'Api\LoginController@getNotification');
	

	Route::post('add-session', 'Api\SessionController@addSession');
	Route::post('update-session', 'Api\SessionController@updateSession');
	Route::post('get_upcoming_session', 'Api\SessionController@getUpcomingClassSessions');
	
	Route::get('get_my_class_subject', 'Api\TeacherClassSubjectController@getMyClassSubject');
	
	//Route::post('get_my_all_session', 'Api\SessionController@getMyAllSession');
	Route::post('add_teacher_class_subject', 'Api\TeacherClassSubjectController@addTeacherClassSubject');
	Route::post('management', 'Api\ManagementController@studentAndTeacherDetails');
	Route::post('add-study-material', 'Api\StudyMaterialController@addStudyMaterial');
	Route::post('get-study-material', 'Api\StudyMaterialController@getStudyMaterial');
	Route::get('get-study-material/{id}', 'Api\StudyMaterialController@getStudyMaterialByid');
	
	Route::post('add-assignment', 'Api\AssignmentController@addAssignment');
	Route::post('get_assignment_list', 'Api\AssignmentController@getAssignmentList');
	Route::get('get_assignment/{id}', 'Api\AssignmentController@getAssignmentById');
	
	
	Route::post('add_forum_post', 'Api\ForumController@addForumPost');
	Route::post('get_forum_post', 'Api\ForumController@getForumPost');
	Route::post('get_forum_last', 'Api\ForumController@getForumLast');

	Route::post('set_as_read', 'Api\ClassController@setRead');
	Route::post('time-table', 'Api\TimeTableController@timeTable');
	Route::post('get_students_for_assingment', 'Api\AssignmentController@studentlistForAssignment');
	
	// Route::post('get-subject', 'Api\SubjectController@getSubjectList');
	Route::post('image-upload', 'Api\ImageController@uploadImage');
	Route::post('file_delete', 'Api\ImageController@fileDelete');
	

	
	Route::post('student-submit-assignment', 'Api\AssignmentController@submitAssignmentByStudent');
	Route::post('set-assignment-remark', 'Api\AssignmentController@submitAssignmentRemark');
	
	Route::post('get_my_created_assingment', 'Api\AssignmentController@getMyCreatedAssingment');
	Route::post('get_assingment_submitted_student_list', 'Api\AssignmentController@getAssingmentSubmittedStudentList');
	Route::post('get_submitted_assigment_student_detail', 'Api\AssignmentController@getsubmittedAssigmentStudentDetail');
	Route::post('get_submitted_assignment_list', 'Api\AssignmentController@getSubmittedAssignmentList');
	Route::post('check_assignment_status', 'Api\AssignmentController@checkAssignmentStatus');
	

	Route::post('delete_assignment', 'Api\AssignmentController@deleteAssignment');
	Route::post('delete_assignment_submittted', 'Api\AssignmentController@deleteAssignmentSubmittted');
	Route::post('delete_forum', 'Api\ForumController@deleteForumPost');
	Route::post('delete_session', 'Api\SessionController@deleteSession');
	Route::post('delete_study_material', 'Api\StudyMaterialController@deleteStudyMaterial');



	Route::post('add_update_quiz_table', 'Api\QuizController@addUpdateQuizTable');
	Route::post('add_update_quiz_detail_table', 'Api\QuizController@addUpdateQuizDetail');
	Route::post('add_update_quiz_result_table', 'Api\QuizController@addUpdateQuizResult');	
	Route::post('get_quiz', 'Api\QuizController@getQuiz');
	Route::post('get_quiz_result', 'Api\QuizController@getQuizResult');
	Route::post('get_quiz_table', 'Api\QuizController@getTableQuiz');

	Route::post('add_notice_board', 'Api\NoticeController@addNotice');
	Route::post('update_notice_board', 'Api\NoticeController@updateNotice');
	

	


	// Route::post('add_teacher_class_subject', 'Api\TeacherClassSubjectController@addTeacherClassSubject');
	Route::post('add_update_time_table', 'Api\TimeTableController@addTimeTable');
	
	Route::post('delete_class_section', 'Api\ClassSectionController@deleteClassSection');
	Route::post('delete_notice', 'Api\NoticeController@deleteNotice');
	Route::post('delete_subject_class', 'Api\SubjectClassController@deleteSubjectClass');
	Route::post('delete_subject', 'Api\SubjectMasterController@deleteSubject');
	Route::post('delete_teacher_class_subject', 'Api\TeacherClassSubjectController@deleteTeacherClassSubject');
	Route::post('delete_time_table', 'Api\TimeTableController@deleteTimeTable');
	Route::post('time-table-form', 'Api\TimeTableController@timeTableForm');	
	Route::post('notice-form', 'Api\NoticeController@noticeForm');
	Route::post('subject-class-form', 'Api\SubjectClassController@subjectClassForm');	
	Route::post('make-notice-list', 'Api\NoticeController@makeNoticeList');

});





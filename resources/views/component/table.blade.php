  <?php 
    function viewMoreAndLess($first, $secondArr = [])
    {
        if(!empty($first)){
          echo $first;
        }
        if(!empty($secondArr)){
         $secondArr=array_unique($secondArr);
            ?>
              <span class="viewLess"><?=$secondArr[0]?>
              <?php if(count($secondArr)>1){?>
                <span class="clickviewmore" title="View More">view more..</span>
              <?php }?>
              </span>
              <span class="viewmore">
                  <?php 
                  echo implode(", ", $secondArr);
                  ?>
                  <span class="clickviewless" title="View Less">view less..</span>
              </span>
        <?php }
    }
    function gettingArray($row){
      $row =(array) $row;
      //$row =$row->toArray();
      return $row;
    }
// echo "<pre>";
// print_r($list->toArray());exit;

    $arrayforViewmoreless=['name'];
    ?>
   <div class="card-body text-right">
      @if($form_Type=="link")
        <a href="{{$link['add_edit_form']}}" class="btn btn-info">Add</a>
      @else
        <button type="button" class="btn btn-info gettingform" data-toggle="modal" data-target="#EditModel" rel="{{$apis['add_edit_form']}}" >  Add </button>
      @endif


    <div class="table-responsive">
          <table id="dataTable" class="display">
                  <thead>
                      <tr>
                          <th>S.No</th>
                          @foreach($columnlist as $key => $val)
                          <th class="text-nowrap">{{$val}}</th>
                           @endforeach
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                    @foreach($list as $key => $row)
                     <?php

                      $row=gettingArray($row);
                      ?>
                      <tr>
                          <td><?= $key+1; ?></td>
                          @foreach($columnlist as $k => $val)
                            <td>{{$row[$k]}}</td>
                          @endforeach
                          <td>
                           @if($form_Type=="link")
                               <a href="{{url(session("role").'/school',[$row['id'],'edit'])}}"  title="Edit">
                                <i class="fa fa-edit"></i>
                              </a>
                           @else
                               <a href="#"  data-toggle="modal" 
                                            title="Edit"
                                            rel="{{$apis['add_edit_form']}}?id={{$row['id']}}"
                                            data-target="#EditModal" >
                                <i class="fa fa-edit"></i>
                              </a>
                            @endif 
                          |
                           <a href="#"  title="Delete"
                               data-toggle="modal" onclick="deleteData({{$row['id']}})" 
                               data-target="#DeleteModal" >
                               <i class="fa fa-trash"></i> 
                            </a>
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
          </table>
    </div>
  </div>


 <div class="modal" id="EditModel">
        <div class="modal-dialog">
        <!-- <button type="button" class="close" data-dismiss="modal" style="color: antiquewhite">&times;</button>  -->
          <div class="modal-content" id='classSectionform'>
           <div class="modal-header bg-danger">
                <span class="modal-title text-center">Class Information</span>
            </div>
             <div class="modal-body">


             </div>
          </div>
        </div>
      </div>



   



@section('scripts')
<script>

function deleteData(id)
{
    var id = id;
    var url = '{{url(session("role").'/school')}}';
    url = url+'/'+id;
    $("#deleteForm").attr('action', url);
}

function formSubmit()
{
    $("#deleteForm").submit();
}
$(document).ready(function() {
    $('#importSchool').on('hidden.bs.modal', function(){
      $('.custom-file-label').text('');
     });
     $('#inputGroupFile02').on('change',function(){
        //get the file name
        var fileName = $(this).val();
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
    })
});
</script>
@endsection
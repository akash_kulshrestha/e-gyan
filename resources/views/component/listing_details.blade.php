@extends('BackEnd/layouts.master')
@section('title')
 Class Management | School App
@endsection
@section('content')

<div class="panel-header panel-header-sm">
</div>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
         @component("component.table",
                      [ 
                        'list'=>$classes,
                        'form_Type'=>"popup",
                        "table_name" => "class",
                        "table_display_name" => "Class",
                        "apis"=>[
                            "add_edit_submit"=>"",
                            "add_edit_form"=>"/api/v1/class-section-form"
                        ],
                        "link"=>[
                            "add_edit_submit"=>"",
                            "add_edit_form"=>""
                        ],
                        "columnlist" => [
                             "class_name"=>"Class Name",
                             "section_name"=>"Section",
                             "teacherName"=>"Teacher",
                          ]
                      ]
          );
         @endcomponent
      </div>
    </div>
  </div>
</div>



 <button type="button"
           data-toggle="modal" 
           title="Add New"
           data-target="#popupModel"
            data="class-section-form"
           class="btn btn-success addnewbutton gettingform">Add New</button>

  <div class="table-responsive">
        <table id="dataTable" class="display">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th class="text-nowrap">Class Name</th>
                        @if($request->role=="admin")
                          <th class="text-nowrap">School Name</th>
                        @endif  
                        <th class="text-nowrap">Section</th>
                        <th class="text-nowrap">Teacher</th>
                        <th class="text-nowrap">Action</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($response as $key=> $res)
                    <tr>
                      <td>{{$key+1}}</td>
                      <td >{{$res->class_name}} </td>
                       @if($request->role=="admin")
                          <td >{{$res->school_name}} </td>
                        @endif 
                      <td>{{$res->section_name}}</td>
                      <td>{{$res->teacher_name}}</td>
                       <td>
                        <i class="fas fa-edit gettingform"
                            data-toggle="modal" 
                            title="Edit"
                            data-target="#popupModel"
                            rel="<?=$res->id?>"
                            data="class-section-form"
                            ></i>

                        <i class="fas fa-trash-alt todelete"
                            title="Delete"
                            rel="<?=$res->id?>" data="class_section"></i>
                      </td>
                  </tr>
                  @endforeach
                </tbody>
        </table>
  </div>
   
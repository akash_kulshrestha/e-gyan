<!-- Modal Header -->
<?php 
   error_reporting(0);
   ?>
<div class="modal-header">
   <h4 class="modal-title">
      <?php 
         if(empty($class_section)){
           echo "Add Class";  
         }else{
           echo "Update Class";
         }
         ?>
   </h4>
   <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<!-- Modal body -->
<div class="modal-body" >
   <form action="update_class_section" method="POST" class="eGyanForm" enctype="multipart/form-data" rel='{{url($request['role'].'/class')}}'>
   <input type="hidden" name="id" value="<?= $class_section->id; ?>"/>
  
   @if(Auth::guard('admin')->check())
      <div class="form-group">
    @else
    <?php  $data->school_id = $request->school_id; ?>
      <div class="hidden">
    @endif
        <label for="class">School <span class="start">*</span></label>
        <select class="form-control" name='school_id' id="selectSchool">
          <option value="">Select School</option>
            @foreach($schools as $school)
              @if($data->school_id == $school->id )
                <option value="{{$school->id}}" selected="{{$school->name}}">{{ucfirst($school->name)}}</option>
              @else
                <option value="{{$school->id}}">{{ucfirst($school->name)}}</option>
              @endif
            @endforeach
        </select>  
      </div>
   <div class="form-group">
      <label for="class">Class <span class="start">*</span></label>
      <input type="text" name="class_name" id="class" class="form-control" placeholder="Enter class name" value="{{$class_section->class_name}}">
   </div>
   <div class="form-group">
      <label for="section">Section <span class="start">*</span></label>
      <input type="text" name="section_name" id="section" class="form-control" placeholder="Enter section name" value="{{$class_section->section_name}}">
   </div>
   <div class="form-group">
      <label for="section">Class Teacher </label>
      <select class="form-control subschoolelement" name="class_teacher_id" id="class_teacher">
         <option value="">Select class teacher</option>
         @if(!empty($teachers))
         @foreach($teachers as $teacher)
         <option class="AllschoolOption selectedschooloption-{{$teacher->school_id}}" <?= ($teacher->id === $class_section->class_teacher_id ? 'selected':'' ) ?> value="<?= $teacher->id; ?>"><?= $teacher->name ?></option>
         @endforeach
         @endif
      </select>
   </div>
   <button class="btn btn-primary float-right submitButton">Save</button>
   </form>
</div>
<script>
  
</script>
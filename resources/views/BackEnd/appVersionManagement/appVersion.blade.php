
 <button type="button"
           data-toggle="modal" 
           title="Add New"
           data-target="#popupModel"
            data="app-version-form"
           class="btn btn-success addnewbutton gettingform">Add New</button>

  <div class="table-responsive">
        <table id="dataTable" class="display">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th class="text-nowrap"> School Name</th>
                        <th class="text-nowrap">App Version</th>
                        <th class="text-nowrap">Status</th>
                        <th class="text-nowrap">Action</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($response as $key=> $res)
                    <tr>
                      <td>{{$key+1}}</td>
                        <td>{{$res->schoolName}}</td>
                        <td>{{$res->app_version}}</td>
                        <td>{{$res->mandatory_status}}</td>
                       <td>
                        <i class="fas fa-edit gettingform"
                            data-toggle="modal" 
                            title="Edit"
                            data-target="#popupModel"
                            rel="<?=$res->id?>"
                            data="app-version-form"
                            ></i>

                        <i class="fas fa-trash-alt todelete"
                            title="Delete"
                            rel="<?=$res->id?>" data="app_version"></i>
                      </td>
                  </tr>
                  @endforeach
                </tbody>
        </table>
  </div>


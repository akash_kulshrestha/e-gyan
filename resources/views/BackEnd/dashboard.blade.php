@extends('BackEnd/layouts.master')
@section('title')
School App
@endsection

@section('content')
<div id="waitingProcess" ><img src='/uploads/other_images/wait.gif' width="64" height="64" /><br>Loading..</div>
<div class="content mainContent">
     <div class="row">
        <div class="col-md-4">
           <div class="box-dashboard">
              <div class="d-flex align-items-center justify-content-between">
                 <div>
                    <h3 class="total-order mt0">Students</h3>
                    <h2 class="color-red total-value mt0 mb0">{{$studentCount}}</h2>
                 </div>
                 <div>
                    <div class="total-order-ico">
                       <img src="<?php echo url('/Assets/img/icon1.png') ?>" alt="" title="">
                    </div>
                 </div>
              </div>
           </div>
        </div>
        <div class="col-md-4">
           <div class="box-dashboard">
              <div class="d-flex align-items-center justify-content-between">
                 <div>
                    <h3 class="total-order mt0">Teachers</h3>
                    <h2 class="color-yellow total-value mt0 mb0">{{$teacherCount}}</h2>
                 </div>
                 <div>
                    <div class="total-order-ico">
                       <img src="<?php echo url('/Assets/img/icon2.png') ?>" alt="" title="">
                    </div>
                 </div>
              </div>
           </div>
        </div>
       
        <?php if( session("role") !="admin") { ?>
        <div class="col-md-4">
           <div class="box-dashboard">
              <div class="d-flex align-items-center justify-content-between">
                 <div>
                    <h3 class="total-order mt0">Live Upcoming Sessions</h3>
                    <h2 class="color-blue total-value mt0 mb0">{{count($UpcommingSession)}}</h2>
                 </div>
                 <div>
                    <div class="total-order-ico">
                       <img src="<?php echo url('/Assets/img/icon3.png') ?>" alt="" title="">
                    </div>
                 </div>
              </div>
           </div>
        </div>


          <?php } else{ ?>
             <div class="col-md-4">
               <div class="box-dashboard">
                  <div class="d-flex align-items-center justify-content-between">
                     <div>
                        <h3 class="total-order mt0"> Total School</h3>
                        <h2 class="color-yellow total-value mt0 mb0">{{$SchoolCount}}</h2>
                     </div>     
                     <div>
                        <div class="total-order-ico">
                            <i style="font-size: 78px;" class="fa fa-university" aria-hidden="true"></i>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php }?>
             <div class="col-md-4">
               <div class="box-dashboard">
                  <div class="d-flex align-items-center justify-content-between">
                     <div>
                        <h3 class="total-order mt0">New Notice</h3>
                        <h2 class="color-yellow total-value mt0 mb0">{{$NoticeCount}}</h2>
                     </div>     
                     <div>
                        <div class="total-order-ico">
                            <i style="font-size: 78px;" class="fa fa-university" aria-hidden="true"></i>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
     </div>
     
  
</div>
@endsection

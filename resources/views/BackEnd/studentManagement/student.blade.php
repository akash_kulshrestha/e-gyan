 <button type="button"
           data-toggle="modal" 
           title="Add New"
           data-target="#popupModel"
            data="student-form"
           class="btn btn-success addnewbutton gettingform">Add New</button>
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#importStudent">
    Import Students
  </button>
  <div class="table-responsive">
        <table id="dataTable" class="display">
                <thead>
                    <tr>
                        <th>S.No</th>
                        @if($request->role=="admin")
                          <th class="text-nowrap">School Name</th>
                        @endif  
                         <th class="text-nowrap" >Student Name</th>
                          <th class="text-nowrap">Registration No.</th>
                          <th class="text-nowrap">Login Id</th>
                          <th class="text-nowrap" >Roll No.</th>
                          <th class="text-nowrap">Class Name</th>
                        <th class="text-nowrap">Action</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($response as $key=> $res)
                    <tr>
                      <td>{{$key+1}}</td>
                       @if($request->role=="admin")
                          <td >{{$res->school_name}} </td>
                        @endif 
                        <td>{{$res->name}}</td>
                        <td>{{$res->registration_no}}</td>
                        <td>{{$res->login_id}}</td>
                        <td>{{$res->roll_no}}</td>
                        <td><?= $res->class_name.'-'.$res->section_name ?></td>
                       <td>
                        <i class="fas fa-edit gettingform"
                            data-toggle="modal" 
                            title="Edit"
                            data-target="#popupModel"
                            rel="<?=$res->id?>"
                            data="student-form"
                            ></i>

                        <i class="fas fa-trash-alt todelete"
                            title="Delete"
                            rel="<?=$res->id?>" data="student"></i>
                      </td>
                  </tr>
                  @endforeach
                </tbody>
        </table>
  </div>
   

<div class="modal" id="importStudent">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Import Students</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
    <form action="{{url(session("role").'/importTeachers')}}" method="POST"  enctype="multipart/form-data">
    {{ csrf_field()}}
      <div class="form-group">
          <label>Select excel file to upload <span class="start">*</span></label>
          <div class="custom-file">
            <input type="file" class="custom-file-input" name="file" id="inputGroupFile02">
            <label class="custom-file-label" for="customFile">Choose file</label>
          </div>
      </div>
      <button type="submit" class="btn btn-primary float-left">Upload</button>
      <br>
      <a href="{{url('/sample/studentReg.xlsx')}}" class="float-right">Download Sample</a>
    </form>
    </div>
  </div>
</div>
      
   

@section('scripts')
<script>
$(document).ready(function() {
    $('#importStudent').on('hidden.bs.modal', function(){
      $('.custom-file-label').text('');
     });
     $('#inputGroupFile02').on('change',function(){
        //get the file name
        var fileName = $(this).val();
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
    })
});
</script>
@endsection
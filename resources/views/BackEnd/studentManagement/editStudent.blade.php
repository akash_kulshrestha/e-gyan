<!-- Modal Header -->
<?php 
   error_reporting(0);
 
   ?>
<div class="modal-header">
   <h4 class="modal-title">
      <?php 
         if(empty($data)){
           echo "Add Student";  
         }else{
           echo "Update Student";
         }
         ?>
   </h4>
   <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<!-- Modal body -->
<div class="modal-body" style="width: 300px">
    <form action="add_update_student" method="POST" class="eGyanForm" enctype="multipart/form-data" >
   <input type="hidden" name="id" value="<?= $data->id; ?>"/>
 
                @if(Auth::guard('admin')->check())
                  <div class="form-group">
                @else
                <?php  $data->school_id = $request->school_id; ?>
                  <div class="hidden">
                @endif
                    <label for="class">School <span class="start">*</span></label>
                    <select class="form-control" name='school_id' id="selectSchool">
                      <option value="">Select School</option>
                        @foreach($schools as $school)
                          @if($data->school_id == $school->id )
                            <option value="{{$school->id}}" selected="{{$school->name}}">{{ucfirst($school->name)}}</option>
                          @else
                            <option value="{{$school->id}}">{{ucfirst($school->name)}}</option>
                          @endif
                        @endforeach
                    </select>  
                  </div>
                  <div class="row">
                    <div class="col-md-6  col-sm-12">
                      <div class="form-group">
                        <label>Name <span class="start">*</span></label>
                        <input type="text" class="form-control" placeholder="Enter Student Name" name="name" value="{{ $data->name }}">
                      </div>
                    </div>
                    <div class="col-md-6  col-sm-12">
                      <div class="form-group">
                        <label>Registration No <span class="start">*</span></label>
                        <input type="text" class="form-control" placeholder="Enter Student Registration Number" name="registration_no" value="{{ $data->registration_no }}">
                      </div>
                    </div>
                 
                    <div class="col-md-6  col-sm-12">
                      <div class="form-group">
                        <label>Class Name <span class="start">*</span></label>
                        <select class="form-control subschoolelement" name="class_section_id" id="ClassSectionId">
                           <option value="">select class and section</option>
                           @if(!empty($classSections))
                           @foreach($classSections as $classSection)
                           <option  class="AllschoolOption selectedschooloption-{{$classSection->school_id}}"
                             {{ $data->class_section_id == $classSection->id ? 'selected' : '' }} value="<?= $classSection->id; ?>"> <?= $classSection->class_name.'-'.$classSection->section_name; ?>
                            </option>
                           @endforeach
                           @endif
                        </select>
                      </div>
                    </div>
                     <div class="col-md-6  col-sm-12">
                      <div class="form-group">
                        <label>Select Optional </label>
                        <select class="form-control subsectionelement" name="optional">
                           <option value="">select subject</option>
                           @foreach($subject_optional as $optional)
                           <option  class="AllClassSectionOption selectedClassSectionoption-{{$optional->class_section_id}}" 
                             <?php echo $data->optional == $optional->id ? 'selected' : '' ?> value="{{$optional->id}}"> {{$optional->subject_name}}
                            </option>
                           @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6  col-sm-12">
                      <div class="form-group">
                      <label>Email</label>
                        <input type="email" class="form-control" name="email" placeholder="Enter Student Email" value="{{ $data->email }}">
                      
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                    <label>Gender<span class="start">*</span></label>    
                      <select class="form-control" name="gender">
                        <option value='male'  <?=($data->gender=='male')?'selected':""?> >Male</option>
                        <option value="female"  <?=($data->gender=='female')?'selected':""?> >Female</option>
                        <option value="trangender" <?=($data->gender=='trangender')?'selected':""?> >Trangender</option>
                      </select>    
                      </div>
                  </div>
                    <div class="col-md-6  col-sm-12">
                      <div class="form-group">
                      <label>Phone No</label>
                        <input type="text" class="form-control" name="contact_num" placeholder="Enter Student Phone No" value="{{ $data->contact_num }}">
                       </div>
                    </div>
                    <div class="col-md-6  col-sm-12">
                      <div class="form-group">
                      <label>Roll No <span class="start">*</span></label>
                        <input type="text" class="form-control" name="roll_no" placeholder="Enter Student Roll No" value="{{ $data->roll_no }}">
                      </div>
                    </div>
                  
                    <div class="col-md-6  col-sm-12">
                      <div class="form-group">
                      <label>Login Id <span class="start">*</span></label>
                        <input type="text" class="form-control" name="login_id" placeholder="Enter Student Login id" value="{{ $data->login_id }}">
                      
                       </div>
                    </div>
                    <div class="col-md-6  col-sm-12">
                      <div class="form-group">
                        <?php ?>
                      <label>Password <span class="start">*</span></label>
                        <input type="password" class="form-control" name="password" placeholder="Set Password"  value="{{ $data->password }}" id="password">
                        <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password" id="show__hide_password"></span>
                      </div>
                    </div>
                    <div class="col-md-6  col-sm-12">
                          <div class="form-group">
                            <label>Date of Birth <span class="start">*</span></label>
                            <div class='input-group date' id="datetimepicker1">
                                <input type='text' class="form-control" name="dob" value="{{ $data->dob }}" id="dob" min="1900-01-01" max=""/>
                                <!-- <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span> -->
                            </div>
                         </div>
                    </div>
                    <div class="col-md-6  col-sm-12">
                        <div class="form-group">
                            <label>Select Profile Pic <span class="start"></span></label>
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="files[]"  id="profilepic">
                              <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                            <image src="{{ $data['file_url']['file_url'] }}" style="width:70px;Height:70px"/>
                        </div>
                    </div>
                        
                  
                    <div class="col-md-6  col-sm-12">
                        <div class="form-group">
                            <label>Address <span class="start">*</span></label>
                            <textarea rows="4" cols="80" class="form-control" name="address" placeholder="Enter student's address" >{{ $data->address }}</textarea>
                        </div>
                    </div>
                   
                   </div>Parent's Details
<hr>
                   <div class="row">
                    <div class="col-md-6  col-sm-12">
                      <div class="form-group">
                      <label>Father's Name <span class="start">*</span></label>
                        <input type="text" class="form-control" name="father_name" placeholder="Enter Student parent Name" value="{{ $data->father_name }}">
                      </div>
                    </div>
                    <div class="col-md-6  col-sm-12">
                      <div class="form-group">
                      <label>Mother's Name <span class="start">*</span></label>
                        <input type="text" class="form-control" name="mother_name" placeholder="Enter Student parent Name" value="{{ $data->mother_name }}">
                      </div>
                    </div>
                   
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-3  col-sm-12">
                        <button class="btn btn-primary btn-block submitButton">Save</button>
                        </div>
                    </div>
                  </form>
</div>


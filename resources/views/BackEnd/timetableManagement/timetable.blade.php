 <button type="button"
           data-toggle="modal" 
           title="Add New"
           data-target="#popupModel"
            data="timetable-form"
           class="btn btn-success addnewbutton gettingform">Add New</button>
  
  <div class="table-responsive">
        <table id="dataTable" class="display">
                <thead>
                    <tr>
                        <th>S.No</th>
                        @if($request->role=="admin")
                          <th class="text-nowrap">School Name</th>
                        @endif  
                         <th class="text-nowrap" >Type</th>
                          <th class="text-nowrap" >Class</th>
                          <th class="text-nowrap" >Content</th>
                        <th class="text-nowrap">Action</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($response as $key=> $res)
                    <tr>
                      <td>{{$key+1}}</td>
                       @if($request->role=="admin")
                          <td >{{$res->school_name}} </td>
                        @endif 
                        <td>{{ucfirst($res['type'])}}</td>
                        <td>{{$res['class_section']['class_name']." ".$res['class_section']['section_name']}}</td>
                        <td>{{$res['content']}}</td>
                       <td>
                         @if(!empty($res['file_url']['file_url']))
                            <a href="{{$res['file_url']['file_url']}}" download="{{$res['file_url']['file_url']}}" class="btn btn-primary a-btn-slide-text">
                                download <i class="fas fa-arrow-alt-from-top"></i>   
                          </a>
                            
                          @endif
                        <i class="fas fa-edit gettingform"
                            data-toggle="modal" 
                            title="Edit"
                            data-target="#popupModel"
                            rel="<?=$res->id?>"
                            data="timetable-form"
                            ></i>

                        <i class="fas fa-trash-alt todelete"
                            title="Delete"
                            rel="<?=$res->id?>" data="time_table"></i>
                      </td>
                  </tr>
                  @endforeach
                </tbody>
        </table>
  </div>
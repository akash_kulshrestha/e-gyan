<!-- Modal Header -->
<div class="modal-header">
   <h4 class="modal-title">
      <?php 
         if(empty($data)){
           echo "Add School";  
         }else{
           echo "Update School";
         }
         ?>
   </h4>
   <button type="button" class="closePopUPp" data-dismiss="modal">&times;</button>
</div>
<!-- Modal body -->
<div class="modal-body" style="width: 300px">
    <form action="schooladdedit" method="POST" class="eGyanForm" enctype="multipart/form-data" >
   <input type="hidden" name="id" value="<?= empty($data->id)?"":$data->id; ?>"/>
 
		<div class="row">
			<div class="col-md-6  col-sm-12">
				 <div class="form-group">
						<label>Name <span class="start">*</span></label>
						<input type="text" class="form-control" placeholder="Enter School Name" name="name" value="{{ $data['name'] }}">
				 </div>
			</div>
			<div class="col-md-6  col-sm-12">
				 <div class="form-group">
						<label>School Url </label>
						<input type="text" class="form-control" placeholder="Enter School Url" name="school_url" value="{{ $data['school_url'] }}">
				 
					</div>
			</div>
				<div class="col-md-6  col-sm-12">
				 <div class="form-group">
						<label>Login Id <span class="start">*</span></label>
						<input type="text" class="form-control" name="login_id" placeholder="Enter School Login id" value="{{ $data['login_id'] }}">
				 </div>
			</div>
			<div class="col-md-6  col-sm-12">
				 <div class="form-group">
						<label>Password <span class="start">*</span></label>
						<input type="password" class="form-control" name="password" placeholder="Set Password"  value="{{ $data['password'] }}" id="password">
						<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password" id="show__hide_password"></span>
				 </div>
			</div>
			
			<div class="col-md-6  col-sm-12">
				 <div class="form-group">
						<label>Phone No <span class="start">*</span></label>
						<input type="text" class="form-control" name="phone_no" placeholder="Enter School Phone No" value="{{ $data['phone_no'] }}">
				 </div>
			</div>
			<div class="col-md-6  col-sm-12">
				 <div class="form-group">
						<label>Email <span class="start">*</span></label>
						<input type="email" class="form-control" name="email" placeholder="Enter School Email" value="{{ $data['email'] }}">
				 </div>
			</div>
		
			<div class="col-md-6  col-sm-12">
				 <div class="form-group">
						<label>Logo </label>
							<div class="custom-file">
								<input type="file" class="custom-file-input" name="files[]" accept=".jpg,.png,.jpeg">
								<label class="custom-file-label" for="customFile">Choose file</label>
							</div>
							<image src="{{ $data['file_url']['file_url'] }}" style="width:70px;Height:70px"/>
				 </div>
			</div>
			<div class="col-md-6  col-sm-12">
				 <div class="form-group">
						<label>Address </label>
						<textarea rows="4" cols="80" class="form-control" name="address" placeholder="Enter School's address" >{{ $data['address'] }}</textarea>
				 </div>
			</div>
	 	</div>
		<div class="row justify-content-center">
				<div class="col-md-3  col-sm-12">
				<button class="btn btn-primary btn-block submitButton">Save</button>
				</div>
		</div>
	</form>
</div>
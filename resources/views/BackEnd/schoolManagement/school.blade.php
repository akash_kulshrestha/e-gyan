
 <button type="button"
           data-toggle="modal" 
           title="Add New"
           data-target="#popupModel"
            data="get-school-form"
           class="btn btn-success addnewbutton gettingform">Add New</button>

  <div class="table-responsive">
        <table id="dataTable" class="display">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th class="text-nowrap"> School Name</th>
                        <th class="text-nowrap">Login ID</th>
                        <th class="text-nowrap">Email</th>
                        <th class="text-nowrap">Contact</th>
                        <th class="text-nowrap">Action</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($response as $key=> $res)
                    <tr>
                      <td>{{$key+1}}</td>
                      <td >{{$res->name}} </td>
                      <td>{{$res->login_id}}</td>
                      <td>{{$res->email}}</td>
                      <td>{{$res->phone_no}}</td>
                       <td>
                        <i class="fas fa-edit gettingform"
                            data-toggle="modal" 
                            title="Edit"
                            data-target="#popupModel"
                            rel="<?=$res->id?>"
                            data="get-school-form"
                            ></i>

                        <i class="fas fa-trash-alt todelete"
                            title="Delete"
                            rel="<?=$res->id?>" data="school"></i>
                      </td>
                  </tr>
                  @endforeach
                </tbody>
        </table>
  </div>

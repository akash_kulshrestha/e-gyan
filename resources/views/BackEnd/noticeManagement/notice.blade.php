<button type="button"
           data-toggle="modal" 
           title="Add New"
           data-target="#popupModel"
            data="notice-form"
           class="btn btn-success addnewbutton gettingform">Add New</button>
  
  <div class="table-responsive">
        <table id="dataTable" class="display">
                <thead>
                    <tr>
                        <th>S.No</th>
                        @if($request->role=="admin")
                          <th class="text-nowrap">School Name</th>
                        @endif  
                        <th>Type</th>
                        <th>Title</th>
                        <th>Message</th>
                        <th class="text-nowrap">Action</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($response as $key=> $res)
                    <tr>
                      <td>{{$key+1}}</td>
                       @if($request->role=="admin")
                          <td >{{$res->school_name}} </td>
                        @endif 
                        <td>{{ucfirst($res->type)}}</td>
                                <td>{{$res->title}}</td>
                                <td>{{$res->message}}</td>
                       <td>
                         <i class="fas fa-edit gettingform"
                            data-toggle="modal" 
                            title="Edit"
                            data-target="#popupModel"
                            rel="<?=$res->id?>"
                            data="notice-form"
                            ></i>

                        <i class="fas fa-trash-alt todelete"
                            title="Delete"
                            rel="<?=$res->id?>" data="notice_board"></i>
                      </td>
                  </tr>
                  @endforeach
                </tbody>
        </table>
  </div>
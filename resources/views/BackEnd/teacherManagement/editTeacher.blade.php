<!-- Modal Header -->
<?php 
   error_reporting(0);
 
   ?>
<div class="modal-header">
   <h4 class="modal-title">
      <?php 
         if(empty($data)){
           echo "Add Teacher";  
         }else{
           echo "Update Teacher";
         }
         ?>
   </h4>
   <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<!-- Modal body -->
<div class="modal-body" style="width: 500px">
        <form action="add_update_teacher" method="POST" class="eGyanForm" >
                <input type="hidden" name="id" value="{{$data->id}}" />
                 <!-- <input type="hidden" name="_method" value="PUT"> -->
                   @if(Auth::guard('admin')->check())
                      <div class="form-group">
                    @else
                    <?php  $data->school_id = $request->school_id; ?>
                      <div class="hidden">
                    @endif
                    <label for="class">School <span class="start">*</span></label>
                    <select class="form-control" name='school_id' id="selectSchool">
                      <option value="">Select School</option>
                        @foreach($schools as $school)
                          @if($data->school_id == $school->id )
                            <option value="{{$school->id}}" selected="{{$school->name}}">{{ucfirst($school->name)}}</option>
                          @else
                            <option value="{{$school->id}}">{{ucfirst($school->name)}}</option>
                          @endif
                        @endforeach
                    </select>  
                  </div>


                <div class="row">

                  <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                      <label>Name <span class="start">*</span></label>
                      <input type="text" class="form-control" placeholder="Enter teacher Name" name="name" value="{{ $data->name }}">
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                    <label>Email <span class="start">*</span></label>
                      <input type="email" class="form-control" name="email" placeholder="Enter teacher Email" value="{{  $data->email  }}">
                      </div>
                  </div>
                  <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                    <label>Gender<span class="start">*</span></label>    
                      <select class="form-control" name="gender" >
                        <option value='male'  <?=($data->gender=='male')?'selected':""?> >Male</option>
                        <option value="female"  <?=($data->gender=='female')?'selected':""?> >Female</option>
                        <option value="trangender" <?=($data->gender=='trangender')?'selected':""?> >Trangender</option>
                      </select>    
                      </div>
                  </div>
                  <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                    <label>Login Id <span class="start">*</span></label>
                      <input type="text" class="form-control" name="login_id" placeholder="Enter teacher Login id" value="{{  $data->login_id }}">
                    
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                    <label>Password <span class="start">*</span></label>
                      <input type="password" class="form-control" name="password" placeholder="Set Password"  value="{{  $data->password }}" id="password">
                      <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password" id="show__hide_password"></span>
                    </div>
                  </div>
               
                  <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                    <label>Employee Id <span class="start">*</span></label>
                      <input type="text" class="form-control" name="employee_id" placeholder="Enter employee id" value="{{$data->employee_id }}">
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                    <label>Mobile No <span class="start">*</span></label>
                      <input type="text" class="form-control" name="phone_no" placeholder="Enter mobile No" value="{{ $data->phone_no}}">
                    </div>
                  </div>
                  
                  <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label>Joining Date </label>
                            <div class='input-group date' id="datetimepicker1">
                                <input type='text' class="form-control" name="date_of_joining" value="{{  $data->date_of_joining  }}" id="date_of_joining"/>
                                <!-- <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span> -->
                            </div>
                        </div>
                  </div>
                  <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label>Date of Birth </label>
                            <div class='input-group date' id="datetimepicker2">
                                <input type='text' class="form-control" name="dob" value="{{ $data->dob }}" id="dob" min="1900-01-01" max=""/>
                                <!-- <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span> -->
                            </div>
                        </div>
                  </div>
                  <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                          <label>Address <span class="start">*</span></label>
                          <textarea rows="4" cols="80" class="form-control" name="address" placeholder="Enter teacher's address" >{{  $data->address  }}</textarea>
                      </div>
                  </div>
                  <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label>Select Profile Pic <span class="start"></span></label>
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="files[]"  id="profilepic">
                              <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                             <image src="{{ $data['file_url']['file_url'] }}" style="width:70px;Height:70px"/>
                        </div>
                  </div>

                 
                 
                  <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                      
                        <label>Class-Section Name <span class="start">*</span></label>
                        <select class="js-example-basic-multiple form-control subschoolelement" name="teacherClassSection[]" multiple="multiple">
                          @if(!empty($classSections))
                           @foreach($classSections as $classSection)
                           <?php
                            if(in_array($classSection->id,$slectedClassSubjects)){ ?>
                                <option selected class="AllschoolOption selectedschooloption-{{$classSection->school_id}}" value="<?= $classSection->id; ?>"><?= $classSection->class_name.'-'.$classSection->section_name.'-'.$classSection->subject_name; ?></option>
                    
                                <?php } else{?>
                    
                                  <option class="AllschoolOption selectedschooloption-{{$classSection->school_id}}" value="<?= $classSection->id; ?>"><?= $classSection->class_name.'-'.$classSection->section_name.'-'.$classSection->subject_name; ?></option>
                          <?php } ?>
                           
                           @endforeach
                           @endif
                        </select>
                      </div>
                  </div>
                 </div>
                  <div class="row justify-content-center">
                     <div class="col-md-3  col-sm-12">
                      <button class="btn btn-primary btn-block submitButton">Update</button>
                     </div>
                  </div>
               
                </form>
                  </div>
               

  <!DOCTYPE html>
  <html lang="en">
 @include("BackEnd.layouts.partials.home.header")
  <?php
    $directoryURI = $_SERVER['REQUEST_URI'];
    $path = parse_url($directoryURI, PHP_URL_PATH);
    $components = explode('/', $path);
    $first_part = $components[2];
    $role= session("role");
    $school_details=session("school_details");
  ?>

  <body class="">
    <div class="">
     @include("BackEnd.layouts.partials.home.left_nev")

      <div class="main-panel" id="main-panel">
        <!-- Navbar -->
          @if($role != 'admin')
            <div class='row fixturesHeading systemcreen'>
            <!--  <div class="col-md-1"></div> -->
            <!--  <div class="col-md-1">
              @if($school_details->file_url)
                  <img id="schoolimage" src="<?=$school_details->file_url->file_url?>" style="height:70px;width: 70px;"/>
               @endif   
              
             </div> -->
            <!--  <div class="col-md-5">
                    <h3><u><i>{{$school_details->name}}</i></u></h3>      
              </div> -->
             <div class="col-md-12">
                 <div class="d-flex align-items-center justify-content-between">
                     <div class="menu-icon"><button type="button" class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></button></div>
                    <div class="header-contact">
                         <div> Phone: <span> <?=$school_details->phone_no?></span></div>
                         <div> Site: <a href="<?=$school_details->school_url?>"><?=$school_details->school_url?></a></div>
                    </div>
                  <a class="logoutbutton systemcreen mt-0" href='{{url("/$role/logout")}}'>
                    logout
                  </a>
              
               </div>
            </div>
          </div>
          @else
          <div class='row fixturesHeading systemcreen'>
             <div class="col-md-10"></div>
             <div class="col-md-2">
                <ul class="navbar-nav">
                <li class="nav-item">
                   <a class="logoutbutton" href='{{url("/$role/logout")}}'>
                    logout
                  </a>
                </li>
              </ul>
             </div>
           </div>
          @endif
          @yield('content')
      </div>
      </div>  
       @include("BackEnd.layouts.partials.home.footer") 
    </body>
      
    
    <div class="modal edit-model" id="popupModel">
    </div>
  </html>

 <div class="sidebar" id="mySidenav" data-color="blue">
      
      <div class="logo text-nowrap">
         
          <a href="#" class="simple-text logo-normal">
            Gyan App
          </a>
      </div>
      <div class="sidebar-wrapper" id="sidebar-wrapper">
        <ul class="nav">
          
          <li  class="leftlink {{ $first_part== 'dashboard' ? 'active' : ''}}">
              <a href='{{url("/$role/dashboard")}}'>
                <i class="now-ui-icons design_app"></i>
                  Dashboard
              </a>
          </li>

          <li id="leftlink_class" class="leftlink {{ $first_part== 'class' ? 'active' : ''}}" rel="get-class-listing-page">
            <a href='#'>
              <i class="now-ui-icons education_atom"></i>
              Class Management
            </a>
          </li>
        
          @if($role ==='admin')
          <li id="leftlink_school" class="leftlink {{ $first_part== 'school' ? 'active' : ''}}" rel="get-school-listing-page">
            <a href='#'>
              <i class="now-ui-icons location_map-big"></i>
              School Management
            </a>
          </li>
          <li id="leftlink_vesion" class="leftlink {{ $first_part== 'app-vesion' ? 'active' : ''}}" rel="get-version-listing-page">
            <a href='#'>
              <i class="now-ui-icons location_map-big"></i>
              App Version
            </a>
          </li>
          @endif
           <li id="leftlink_subject" class="leftlink {{ $first_part== 'subject' ? 'active' : ''}}" rel="get-subject-listing-page" >
            <a href='#'>
              <i class="now-ui-icons location_map-big"></i>
              Subject Master
            </a>
          </li>
          <li id="leftlink_mapping" class="leftlink {{ $first_part== 'mapping' ? 'active' : ''}}" rel="get-class-subject-listing-page">
            <a href='#'>
              <i class="now-ui-icons location_map-big"></i>
              Class Subjects
            </a>
          </li>
          <li id="leftlink_student" class="leftlink {{ $first_part== 'student' ? 'active' : ''}}" rel="get-student-listing-page">
             <a href='#'>
              <i class="now-ui-icons ui-1_bell-53"></i>
              Student Management
            </a>
          </li>
          <li id="leftlink_teacher" class="leftlink {{ $first_part== 'teacher' ? 'active' : ''}}" rel="get-teacher-listing-page">
            <a href='#'>
              <i class="now-ui-icons users_single-02"></i>
              Teacher Management
            </a>
          </li>
          <li id="leftlink_timetable" class="leftlink {{ $first_part== 'timetable' ? 'active' : ''}}" rel ="get-timetable-listing-page">
            <a href='#'>
              <i class="now-ui-icons users_single-02"></i>
              Time Table Management
            </a>
          </li>
          <li  id="leftlink_notice" class="leftlink {{ $first_part== 'notice' ? 'active' : ''}}" rel="get-notice-listing-page">
            <a href='#'>
              <i class="now-ui-icons ui-1_bell-53"></i>
              Notice Board
            </a>
          </li>
        </ul>
      </div>
  </div>


      <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute " 
  id="mobilenavigation" >
           <div class='row'>
            <div class="col-md-12">
              <table class="fixturesHeading">
                <tr>
                  <td>
                     @if($role != 'admin')
                        <div class='row'>
                         <!--<div class="col-md-2">-->
                         <!-- @if($school_details->file_url)-->
                         <!--     <img id="schoolimage" src="<?=$school_details->file_url->file_url?>" style="height:70px;width: 70px;"/>-->
                         <!--  @endif   -->
                         <!--</div>-->
                         <!--<div class="col-md-5">-->
                         <!--       <h6><u><i>{{$school_details->name}}</i></u></h6>      -->
                         <!-- </div>-->
                         <div class="col-md-4 mbl-contact">
                           <?=$school_details->phone_no?> 
                          <br>
                         <a style="color: blue" href="<?=$school_details->school_url?>"><?=$school_details->school_url?></a>
                        </div>
                         <div class="col-md-1"></div>
                      </div>
                      @endif
                  </td>
                  <td>
                       <div class='row'>
                         <div class="col-md-6 text-right">
                               <a class="logoutbutton mobile-btn" href='{{url("/$role/logout")}}'>
                                logout
                              </a>
                         </div>
                         <div class="col-md-6 text-right mbl-menubar">
                            <span class="opensidenev" onclick="openNav()"> <i class="fas fa-bars"></i></span>
                            <span class="closesidenev" onclick="closeNav()"><i class="fas fa-times"></i></span>
                         </div>
                       </div>
                  </td>
                </tr>
              </table>
            </div>
           </div>


         
      </nav>
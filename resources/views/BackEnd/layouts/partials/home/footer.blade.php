	<footer >
		
			<div class="row footer-bottom-link">
						<div class="col-md-12">
							<div class="copyright-text">
								Copyright &copy; {{ date('Y') }} All Right Reserved. 
							</div>
						</div>
				</div>
		  
	   </footer>
		   	
		  

 	<input type="hidden" id="authToken" value="<?php echo Auth::guard($role)->user()->token ?>" />
	<input type="hidden" id="user_role" value="<?php echo $role ?>" />

    <!--   Core JS Files   -->
     <script src="/Assets/js/jquery-3.5.0.min.js" 
    integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="
    crossorigin="anonymous"></script>
    
    <script src="/Assets/js/moment.js"></script>
    <script src="/Assets/js/core/popper.min.js"></script>
    <script src="/Assets/js/core/bootstrap.min.js"></script>
    <script src="/Assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
    <!-- Chart JS -->
    <script src="/Assets/js/plugins/chartjs.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="/Assets/js/plugins/bootstrap-notify.js"></script>
    <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="/Assets/js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script><!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
    <script src="/Assets/js/dataTable.min.js"></script>
    <script src="/Assets/js/select2.js"></script>
    <script src="/Assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/Assets/js/jquery-ui.min.js"></script>
    <script src="/Assets/js/toastr.js"></script>
    <script src="/Assets/js/script.js"></script>
    <script src="/Assets/js/jquery.sumoselect.min.js"></script>
    <script src="/Assets/js/datepicker.js"></script>
    
     @yield('scripts')
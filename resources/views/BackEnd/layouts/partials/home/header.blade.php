 <head>
    <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="76x76" href="/Assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?php echo url('/') ?>/Assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        @yield('title')
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link href="/Assets/releases/latest/css/all.css" rel="stylesheet" />
    <link href="/Assets/css/googleapis.css" rel="stylesheet" />
    <link href="/Assets/css/css.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link href="/Assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/Assets/css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="/Assets/css/dataTable.min.css" rel="stylesheet" />
    <link href="/Assets/demo/demo.css" rel="stylesheet" />
    <link href="/Assets/css/select2.css" rel="stylesheet" />
    <link href="/Assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="/Assets/css/toastr.css" rel="stylesheet" />
    <link href="/Assets/css/sumoselect.min.css" rel="stylesheet" />
    <link href="/Assets/css/datepicker.css" rel="stylesheet" />
    <link href="/Assets/css/style.css" rel="stylesheet" />
</head>
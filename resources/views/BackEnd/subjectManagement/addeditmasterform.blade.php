    <!-- Modal Header -->
    <?php 
      error_reporting(0);
    ?>
    <div class="modal-header">
      <h4 class="modal-title">
      
        <?php 
          if(empty($subject_master)){
            echo "Add Subject";  
          }else{
            echo "Update Subject";
          }
        ?>
      </h4>
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    
    <!-- Modal body -->
    <div class="modal-body" >
          
                <form action="update_subject_master" method="POST" class="eGyanForm" enctype="multipart/form-data">
                <input type="text" class="hidden" name="id" value="<?= $subject_master->id; ?>"/>
                @if(Auth::guard('admin')->check())
                  <div class="form-group">
                @else
                <?php  $subject_master->school_id = $request->school_id; ?>
                  <div class="hidden">
                @endif
                    <label for="class">School <span class="start">*</span></label>
                    <select class="form-control" name='school_id' id="selectSchool">
                      <option value="">Select School</option>
                        @foreach($schools as $school)
                          @if($subject_master->school_id == $school->id )
                            <option value="{{$school->id}}" selected="{{$school->name}}">{{ucfirst($school->name)}}</option>
                          @else
                            <option value="{{$school->id}}">{{ucfirst($school->name)}}</option>
                          @endif
                        @endforeach
                    </select>  
                  </div>

                  <div class="form-group">
                    <label for="class">Subject <span class="start">*</span></label>
                    <input type="text" name="subject_name" class="form-control" value="{{ $subject_master->subject_name }}" placeholder="Enter subject name" id="subject">
                  </div>
                <button class="btn btn-primary float-right submitButton">Save</button>
          </form>

    </div> 

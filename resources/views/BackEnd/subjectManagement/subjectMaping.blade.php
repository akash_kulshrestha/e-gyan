 <button type="button"
           data-toggle="modal" 
           title="Add New"
           data-target="#popupModel"
            data="class-subject-form"
           class="btn btn-success addnewbutton gettingform">Add New</button>

  <div class="table-responsive">
        <table id="dataTable" class="display">
                <thead>
                    <tr>
                        <th>S.No</th>
                         @if($request->role=="admin")
                          <th class="text-nowrap">School Name</th>
                        @endif  
                        <th class="text-nowrap">Class-Section</th>
                        <th class="text-nowrap">Subject</th>
                        
                        <th class="text-nowrap">Optional</th>                       
                        <th class="text-nowrap">Action</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($response as $key=> $res)
                    <tr>
                       <td>{{$key+1}}</td>
                       @if($request->role=="admin")
                          <td >{{$res->school_name}} </td>
                        @endif 

                       <td >{{$res->class_name}} {{$res->section_name}} </td>
                       <td >{{$res->subject_name}} </td>
                       <td >{{$res->optional?"YES":"NO"}} </td>
                       <td>
                        <i class="fas fa-edit gettingform"
                            data-toggle="modal" 
                            title="Edit"
                            data-target="#popupModel"
                            rel="<?=$res->id?>"
                            data="class-subject-form"
                            ></i>
                        <i class="fas fa-trash-alt todelete"
                            title="Delete"
                            rel="<?=$res->id?>" data="subject_class"></i>
                      </td>
                  </tr>
                  @endforeach
                </tbody>
        </table>
  </div>

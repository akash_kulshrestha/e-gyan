 <?php 
      error_reporting(0);
    ?>
<!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title"><?php 
         
                  if(!empty($subject_class)){
                   echo "Update Subject Mapping";
                  }else{
                    echo "Add Subject Mapping";
                  }
              ?></h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
            <form action="add_update_subject_class" method="POST" class="eGyanForm" enctype="multipart/form-data" >
              <input type='hidden' name="id" value="{{$subject_class->id}}" />
              @if(Auth::guard('admin')->check())
                  <div class="form-group">
                @else
                <?php  $subject_class->school_id = $request->school_id; ?>
                  <div class="hidden">
                @endif
                    <label for="class">School <span class="start">*</span></label>
                    <select class="form-control" name='school_id' id="selectSchool">
                      <option value="">Select School</option>
                        @foreach($schools as $school)
                          @if($subject_class->school_id == $school->id )
                            <option value="{{$school->id}}" selected="{{$school->name}}">{{ucfirst($school->name)}}</option>
                          @else
                            <option value="{{$school->id}}">{{ucfirst($school->name)}}</option>
                          @endif
                        @endforeach
                    </select>  
                  </div>


                <div class="form-group">
                    <label>Class-Section Name <span class="start">*</span></label>
                    <select class="form-control" name="class_section_id">
                        <option value="">Select class and section</option>
                        @if(!empty($classSections))
                        @foreach($classSections as $classSection)
                        @if($subject_class->class_section_id)
                        <option {{($subject_class->class_section_id == $classSection->id ? 'selected':'' ) }} value="<?= $classSection->id; ?>"><?= $classSection->	class_name.'-'.$classSection->section_name; ?></option>
                        @else
                        <option value="<?= $classSection->id; ?>"><?= $classSection->	class_name.'-'.$classSection->section_name; ?></option>
                        @endif
                        @endforeach
                        @endif
                    </select>
                </div>
                <div class="form-group">
                  <label>Subject Name <span class="start">*</span></label>
                  
                  <select class="form-control" name="subject_id">
                      <option value="">Select subject</option>
                    
                      @foreach($subjects as $subject)
                        @if(intval($subject_class->subject_id) == intval($subject->id)) 
                          <option  selected value="<?= $subject->id; ?>"><?= $subject->subject_name; ?>
                        </option>
                        @else
                         <option value="<?= $subject->id; ?>"><?= $subject->subject_name; ?>
                        </option>
                        @endif
                      @endforeach
                     
                  </select>
                </div>
                  <div class="form-group">
                    <label>Optional <span class="start">*</span></label>
                    <select class="form-control" name="optional" >
                           <option value="0" <?php ($subject_class->optional == 0) ? 'selected':''?> >No</option>
                            <option value="1" <?php ($subject_class->optional == 1) ? 'selected':''?> >Yes</option>
                    </select>
                </div>
              <button type="submit" class="btn btn-primary float-right submitButton">Save</button>
            </form>
            </div>
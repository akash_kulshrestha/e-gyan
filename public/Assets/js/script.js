window.localStorage.setItem('AuthKey',$("#authToken").val())
 
 $(document).ajaxStart(function(){
    $("#waitingProcess").css("display", "block");
  });
  $(document).ajaxComplete(function(){
    $("#waitingProcess").css("display", "none");
  });


$(document).ready(function(){
  var today = new Date();
    var dtToday = new Date();
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    var maxDate = year + '-' + month + '-' + day;    
    $("body").on('focus',"#date_of_joining", function(){
    $(this).datepicker({
    format: "yyyy/mm/dd",
    autoclose:true,
    monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    });
    });

    $('#dob').attr('max', maxDate);
    $("body").on('focus',"#dob", function(){
        $(this).datepicker({
        format: "yyyy/mm/dd",
        autoclose:true,
        endDate: "today",
        maxDate: today,
        monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        }).on('changeDate', function (ev) {
          $(this).datepicker('hide');
        });
     });
});




var currenttable=window.localStorage.getItem('currenttable') ?window.localStorage.getItem('currenttable'):null;

$(document).on("click",".clickviewmore",function(){
    $(this).closest("td").find(".viewmore").show();
    $(this).closest("td").find(".viewLess").hide();
});

$(document).on("click",".clickviewless",function(){
    $(this).closest("td").find(".viewLess").show();
    $(this).closest("td").find(".viewmore").hide();
});

$('body').on('change', '.errorRed', function() {
    $(this).next('.errormess').remove();
    $(this).removeClass('errorRed');
});
 $(".closesidenev").hide();
function openNav() {
     $(".opensidenev").hide();
     $(".closesidenev").show();
     $("#mySidenav").show();
   document.getElementById("mySidenav").style.width = "300px";
}

$(document).on("change","#ClassSectionId",function(){
  var sectionid= $(this).val();
 
  $(".AllClassSectionOption").hide();
  if($(".subsectionelement")[0]){
     $(".subsectionelement")[0].sumo.selectItem("");
  }
   $(".selectedClassSectionoption-"+sectionid).show();
   $(".subsectionelement").val("");
  
});


$(document).on("change","#selectSchool",function(){
  var schoolid= $(this).val();
  $(".AllschoolOption").hide();
  if($(".subschoolelement")[0]){
     $(".subschoolelement")[0].sumo.selectItem("");
  }else if($(".subschoolelement")){
     $(".subschoolelement").sumo.selectItem("");
  }
   $(".selectedschooloption-"+schoolid).show();
   $(".subschoolelement").val("");
  
});

function closeNav() {
   $(".opensidenev").show();
   $(".closesidenev").hide();
   $("#mySidenav").hide();
}

$(document).on("click","#show__hide_password",function(){
  var obj= $("#password");
  if($(this).hasClass("fa-eye")){
    $(this).removeClass("fa-eye");
    $(this).addClass("fa-eye-slash");
    $(obj).attr('type','text');
  }else
  {
    $(this).removeClass("fa-eye-slash");
    $(this).addClass("fa-eye");
    $(obj).attr('type','password');
  }
});

 function loaddatatable(res){
   $('#dataTablepopup').DataTable(); 
   $('#dataTable').DataTable(); 
   $('select').SumoSelect({search: true});
 }
 if($(window).width() > 1024)
      $("#mobilenavigation").hide();
else{
  $("#mySidenav").hide();
}

  if(currenttable!==null){
      $("#"+currenttable).trigger("click");
  }

 $(".leftlink").click(function(){
     $(".leftlink").removeClass("active");
     $(this).addClass("active");
     if($(window).width() <= 1024)
         closeNav();
     currenttable=$(this); 
     window.localStorage.setItem('currenttable',$(this).attr("id"));
     var url= $(this).attr("rel");
     ajaxCallHtmlupdatelisting(url,{},method='post',$(".mainContent"));
    
 });

 $(document).on("click",".todelete",function(){
   event.preventDefault();
   data={
     id:$(this).attr("rel"),
     table:$(this).attr("data")
    };
    if(confirm("Are you sure to delete?")){
        ajaxCallHtmlupdatelisting("todelete",data,"post");
        toastr.error("deleted successfully");
        $(currenttable).trigger("click");
    }
})


$('body').on('click','.gettingform',function(){
    var url   =   $(this).attr('data');
    var data= {};
    if($(this).attr('rel') > 0 ){
      data.id = $(this).attr('rel');
    }
    ajaxCallHtmlupdatelisting(url,data,'post',$("#popupModel"));
});


function makeNoticeHtml(data){
    var html='';
    for(var i=0;i<data.length;i++){
        var newDate=formatDate(data[i].date);
        html +='<div class="box"><p class="text-right small">'+newDate+'</p><h3>'+data[i].title+' </h3><p>'+data[i].message+'</p><p class="text-right mb-0 mt-1 name"><i class="now-ui-icons users_circle-08"></i> <b>'+data[i].type+'</b> </p></div>';
    }
    return html;
}

$('body').on('click', '.modal-backdrop', function() {
  $("#popupModel").hide();
  $(".modal-backdrop").hide();
  $(".closePopUPp").trigger("click");

})

$('body').on('click', '.submitButton', function() {
  event.preventDefault();
  var form = $('.eGyanForm')[0];
  var data = new FormData(form);
  url = $(form).attr('action');
  response = ajaxCallMultipartForm(url, data);
});

function getHeader(){
  const headers = {
       
        Authorization : "Bearer "+ window.localStorage.getItem('AuthKey')
    }
    return headers;
}

 function ajaxCallHtmlupdatelisting(url,data,method='post',inputfield=""){  

     var baseApiUrl = "/api/v1/";
     url= baseApiUrl+url;
    $.ajax({
         url: url,
         type: method,
         headers:getHeader(),
         data: data,

         success: function(response)
         { 
           if(inputfield!=""){
                $(inputfield).html("");
                $(inputfield).html(response);
                 loaddatatable(data);
           }
           
         }, 
         error: function(XMLHttpRequest, textStatus, errorThrown) { 
           
         }  
   });
}


function ajaxCallMultipartForm(url, data,clickon="") {
  var response = [];
  var baseApiUrl = "/api/v1/";
   url= baseApiUrl+url;
  $.ajax({
      type: "POST",
      enctype: 'multipart/form-data',
      url: url,
      headers:getHeader(),
      data: data,
      processData: false,
      contentType: false,
      async: true,
      cache: false,
      timeout: 600000,  
      success: function(data) {
          if(data.success)
          {
            if(data.message){
               toastr.success(data.message);
               $(".closePopUPp ,.close").trigger("click");
               $(currenttable).trigger("click");
            }
              
            if(clickon !="" ){
              toastr.success("Done successfully");
              $(clickon).trigger("click");
            }
          }else{
              hendleError(data);
          }
      },
      error: function(e) {
          console.log("ERROR : ", e);
          hendleError(e.responseJSON);
      }
  });
  return response;
}


function hendleError(data) {
  $('.errormess').remove();
  if (Object.prototype.toString.call(data.error) == '[object String]' && data.error) {
    toastr.error(data.error);
  } else {
      console.log(data.error);
      $.each(data.error, function(key, value) {
          // if($('.sumo_' + key ).length)
          //    object = $('.sumo_' + key + ']');
          // else 
            if ($('select[name=' + key + ']').length)
              object = $('select[name=' + key + ']');
          else if ($('textarea[name=' + key + ']').length)
              object = $('textarea[name=' + key + ']');
          else
              object = $('input[name=' + key + ']');
          object.addClass('errorRed');
          object.after("<span class='errormess' style='width:100%'>" + value + " </span>")
      });
  }
}
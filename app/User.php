<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use DB;
use Auth;
use Hash;
use Session;
use Illuminate\Validation\Rule;
use JWTAuth;
use App\School, App\Admin;
use App\Student, App\Teacher, App\Teacher_Class_Subject;
use App\Attachments_Table;

use function GuzzleHttp\json_decode;

class User extends Authenticatable
{
  use Notifiable;
  protected $softDelete = true;
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name', 'email', 'password', 'email_verified_at', 'password', 'remember_token', 'pass_code', 'device_token', 'updated_at'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'email_verified_at' => 'datetime',
  ];

  public function file_url()
  {
    return $this->hasMany('App\Attachments_Table', 'reference_id', 'id')->where('table_type', $this->table)->where('status', 1);
  }

  public function getTable()
  {
    return $this->table;
  }

  public static function getId($request)
  {
    return Auth::guard($request->role)->user()->id;
  }

  public static function getPrifix($request)
  {
    $role = $request->route()->getPrefix();
    return explode("/", $role);
  }

  public static function getclass_section($request)
  {
    if ($request->teacher_class_subject_id) {
      $class_section_details = Teacher_Class_Subject::select('class_section.*')
        ->join('subject_class', 'subject_class.id', '=', 'teacher_class_subject.subject_class_id')
        ->join('class_section', 'class_section.id', '=', 'subject_class.class_section_id')
        ->where('teacher_class_subject.id', $request->teacher_class_subject_id)
        ->first();
      return $class_section_details;
    }
  }

  public static function getRequestRole()
  {
    global $request;
    $userType = $request->role;
    if (!empty($request->all()["role"])) {
      $userType = $request->all()["role"];
    }
    return $userType;
  }

  public function getUserObject($userType, $NotformHeader = true)
  {
    global $request;
    if (!empty($request->all()["role"]) && $NotformHeader) {
      $userType = $request->all()["role"];
    }
    if ($userType == 'teacher') {
      $objUser = new Teacher();
    } else if ($userType == 'student') {
      $objUser = new Student();
    } else if ($userType == 'admin') {
      $objUser = new Admin();
    } else if ($userType == 'user') {
      $objUser = new User();
    } else if ($userType == 'school') {
      $objUser = new School();
    }
    return $objUser;
  }

  public function makeUserSession($user)
  {
    global $request;
    session(["role" => $request->role]);
    session(["user_details" => $user]);
    session(["user_school_id" => '']);
    if ($request->role != 'admin') {
      session(["user_school_id" => $user->id]);
      if ($request->role != 'school')
        session(["user_school_id" => $user->school_id]);
      session(["school_details" => (new School())->getAllDetails(session("user_school_id"))]);
    }
  }


  public function getschoolinfo($request, $userType)
  {
    $objUser = $this->getUserObject($userType);
    $user = $objUser->select("name", "school_id")->where('login_id', $request['login_id'])->first();
    $objSchool = new School();
    if ($user) {
      $response["user"] = $user;
      $response["school"] = $objSchool->getSchoolinfo($user);
      return $response;
    }
    return null;
  }


  public function getsignUpRules($request)
  {

    $userType = "";
    $userType = $request->role;

    $rules = ['name' => "required|string"];
    $rules['email'] = "required|email";
    $rules['password'] = 'required|min:3|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/';
    $rules['login_id'] = "required";

    if ($userType == 'student') {
      if (!$request->user_id) {
        $rules['school_id'] = "required|numeric";
        $rules['registration_no'] = "required";
      }
      $rules['roll_no'] = "required";
      $rules['class_section_id'] = "required|numeric";
      $rules['father_name'] = "required|string";
      $rules['mother_name'] = "required|string";
      $rules['contact_num'] = "required|numeric|min:10";
      $rules['dob'] = "required|date";
      $rules['address'] = "required";
    } else if ($userType == 'teacher') {
      if (!$request->user_id) {
        $rules['employee_id'] = "required";
      }
      $rules['school_id'] = "required|numeric";
      $rules['date_of_joining'] = "required|date";
      $rules['phone_no'] = "required|numeric|min:10";
      $rules['address'] = "required";;
    } else if ($userType == 'school') {
      $rules['phone_no'] = "required|numeric|min:10";
      $rules['address'] = "required";

      //   $rules['image'] = 'mimes:jpeg,jpg,png,gif|required|max:10000';
    }
    return $rules;
  }

  public function getUserDetails($id, $userType)
  {
    $objUser = $this->getUserObject($userType,false);
    $data = $objUser->with('file_url')->find($id);
    unset($data['pass_code'], $data['password'],$data['token'],$data['device_token']);
    return $data;
  }


  public  function CheckUserAuth($request)
  {
    try {
      $header = $request->header('Authorization');
      $token = explode(" ", $header)[1];
      $header = json_decode($this->string_decrypt($token), true);
      if ($header) {
        $objUser = $this->getUserObject($header[0], false);
        $user = $objUser->where('token',$token)->where(["login_id" => $header[1], "pass_code" => $header[2]])->first();
        
        if (!empty($user)) {
          $request->role = $header[0];
          if ($request->role == "school") {
            $request->school_id = $user->id;
          } else {
            $request->school_id = $user->school_id ? $user->school_id : $request->school_id;
          }
          $request->user_id = $user->id;
          $request->user_login_id = $user->login_id;

          $request["role"] = $request->role;
          $request["school_id"] = $request->school_id;
          $request["user_id"] = $request->user_id;
          $request["user_login_id"] = $request->user_login_id;

          if($request->role =='teacher' && empty($request->teacher_class_subject_id)){
            $objTeacher_Class_Subject = new Teacher_Class_Subject();
            $data = $objTeacher_Class_Subject->getList($request);  
            $request['teacher_class_subject_id']= $data->pluck('id')->toArray();
          }
          if($request->role =='student'){
            $objUser = new User();
            $objTeacher_Class_Subject=new Teacher_Class_Subject();
            $data = $objUser->getStudentDetails($request->user_id); 
            if(empty($request->class_section_id)){
              $request['class_section_id'] = $data['class_section']['id'];
            }
            if(empty($request->subject_ids)){
              $request['subject_ids']= array_column($data['subjects'],'id'); 
            }
            if(empty($request->teacher_class_subject_id))
                $request['teacher_class_subject_id'] = $objTeacher_Class_Subject->getTeacherClassSubjectIds($request['class_section_id'],$request['subject_ids'], $request->school_id);
          }
          $user->password = $this->string_decrypt($user->pass_code);
          $this->makeUserSession($user);
          if (Auth::guard($request->role)->attempt(['login_id' => $header[1], 'password' =>  $this->string_decrypt($header[2]) ]))
            return true;
        }
      }
    } catch (\Exception $e) {
    }
    return false;
  }

  public function getUserLogin($request, $userType)
  {
    $returnData = [];
    $objUser = $this->getUserObject($userType);
    $condition = ['login_id' => $request->login_id, 'password' => $request->password];
    $school = School::where('unique_sort_name', explode("-", $request->login_id)[0])->first();
    if (!empty($school)) {
      $request->school_id = $school->id;
      $condition['school_id'] = $request->school_id;
    } else if (!empty($request->school_id)) {
      $condition['school_id'] = $request->school_id;
    }
    if (Auth::guard($userType)->attempt($condition)) {
      $user = $objUser->where('login_id', $request->login_id)->first();
      $payload = [$userType, $user['login_id'], $user['pass_code'],$user['updated_at']];
      $user->token = $this->string_encrypt(json_encode($payload));
      if (!empty($request->device_token)) {
        $user->device_token = $request->device_token;
      }
      $user->save();
      $this->makeUserSession($user);
      $returnData = $this->getUserAllDetails($user->id, $userType);
      $returnData['role'] = $userType;
      unset($returnData['password'], $returnData['pass_code']);
    }
    return $returnData;
  }

  function getStudentDetails($id){
    $user = Student::with('file_url', 'class_section')->find($id);
    $subject_class_data = Subject_Class::select('subject_master.id', 'subject_master.subject_name')
      ->leftJoin('subject_master', 'subject_master.id', '=', 'subject_class.subject_id')
      ->where('subject_class.class_section_id', $user->class_section_id)
      ->get();
      $userOthetDetails['subjects']=  $subject_class_data->toArray(); 
    return array_merge($user->toArray(), $userOthetDetails);
  }

  public function getUserAllDetails($id, $userType)
  {
    $returnData = [];
    $userOthetDetails = [];
    if ($userType == 'teacher') {
      $user = Teacher::with('file_url',"school")->find($id);
      $teacher_class_subject = Teacher_Class_Subject::select(
        'teacher_class_subject.id',
        'teacher_class_subject.subject_class_id   as subject_class_id',
        'subject_master.id as subject_id',
        'subject_master.subject_name',

        'class_section.id as class_section_id',
        'class_section.class_name',
        'class_section.section_name'
      )
        ->leftJoin('subject_class', 'subject_class.id', '=', 'teacher_class_subject.subject_class_id')
        ->leftJoin('subject_master', 'subject_master.id', '=', 'subject_class.subject_id')
        ->leftJoin('class_section', 'class_section.id', '=', 'subject_class.class_section_id')
        ->where('teacher_class_subject.teacher_id', $user->id)
        ->where('teacher_class_subject.school_id', $user->school_id)
        ->where('teacher_class_subject.status', 1)
        ->get();
      $userOthetDetails['teacher_class_subject'] = $teacher_class_subject->toArray();
    } else if ($userType == 'student') {
      $user = Student::with('file_url', 'class_section','school')->find($id);
      $subject_class_data = Subject_Class::select('subject_master.id', 'subject_master.subject_name')
        ->leftJoin('subject_master', 'subject_master.id', '=', 'subject_class.subject_id')
        ->where('subject_class.school_id', $user->school_id)
        ->where('subject_class.class_section_id', $user->class_section_id)
        ->get();
      $userOthetDetails['subjects'] = $subject_class_data->toArray();
    } else if ($userType == 'user') {
      $user = self::with('file_url')->find($id);
    } else if ($userType == 'school') {
      $user = School::with('file_url')->find($id);
    } else if ($userType == 'admin') {
      $user = Admin::with('file_url')->find($id);
    }
    $user = $user->toArray();
    if (!empty($user["pass_code"])) {
      $user['password'] = $this->string_decrypt($user["pass_code"]);
    }
    $returnData = array_merge($user, $userOthetDetails);
    return $returnData;
  }

  public function getAllUsers($request)
  {
    if ($request->role == 'teacher') {
      $objTeacher = Teacher::select('id', 'name', 'login_id', 'employee_id');
      if (!empty($request->page_order)) {
        $objTeacher->orderby($request->page_order[0], $request->page_order[1]);
      }
      if (!empty($request->page_limit)) {
        $data = $objTeacher->paginate($request->page_limit)->toArray();
        foreach ($data['data'] as $key => $value) {
          $data['data'][$key]['class_section_and_subject_name'] = $this->getclass_section_and_subject_name($value['id']);
        }
      } else {
        $data = $objTeacher->get()->toArray();
        foreach ($data as $key => $value) {
          $data[$key]['class_section_and_subject_name'] = $this->getclass_section_and_subject_name($value['id']);
        }
      }
    } else if ($request->role == 'student') {
      $objStudent = Student::select('id', 'name', 'login_id', 'registration_no', 'roll_no', 'class_section_name');
      if (!empty($request->page_order)) {
        $objStudent->orderby($request->page_order[0], $request->page_order[1]);
      }
      if (!empty($request->page_limit)) {
        $data = $objStudent->paginate($request->page_limit)->toArray();
      } else {
        $data = $objStudent->get()->toArray();
      }
    }
    return $data;
  }

  public function getclass_section_and_subject_name($id)
  {
    if ($id) {
      $data = Teacher::select(DB::raw("CONCAT(class_section.class_name,'-',class_section.section_name,'-',subject_master.subject_name) as Class_section_And_Subject_name"))
        ->join('teacher_class_subject', 'teacher_class_subject.teacher_id', '=', 'teacher.id')
        ->join('subject_class', 'subject_class.id', '=', 'teacher_class_subject.subject_class_id')
        ->join('class_section', 'class_section.id', '=', 'subject_class.class_section_id')
        ->join('subject_master', 'subject_master.id', '=', 'subject_class.subject_id')
        ->where('teacher.id', $id)->get();
      return $data;
    }
  }

  public function createUpdateUser($request, $userType)
  {

    $objUser = $this->getUserObject($userType);
    if ($request->school_id) {
      $school = School::find($request->school_id);
    }

    if (!empty($request->user_id)) {
      $id = $request->user_id;
      $insertData = $objUser->find($id);
      $login_id = $insertData->login_id;
      $password = $this->string_decrypt($insertData->pass_code);
    } else {
      if ($school && $request["registration_no"]) {
        $login_id = $request["login_id"] = strtoupper($school->unique_sort_name) . "-" . $request["registration_no"];
        $password = $request["password"] = strtoupper($school->unique_sort_name) . "@" . $request["registration_no"];
      } else if ($school && $request["employee_id"]) {
        $login_id = $request["login_id"] = strtoupper($school->unique_sort_name) . "-" . $request["employee_id"];
        $password = $request["password"] = strtoupper($school->unique_sort_name) . "@" . $request["employee_id"];
      }

      if (!empty($request["password"])) {
        $request["pass_code"] = $this->string_encrypt($request["password"]);
        $request["password"] = Hash::make($request["password"]);
      }


      $insertData = [];
      if ($userType == 'teacher' || $userType == 'student') {
        $user = $objUser->where(['school_id' => $request["school_id"], 'login_id' => $request["login_id"]])->first();
      } else {
        $user = $objUser->where(['login_id' => $request["login_id"]])->first();
      }
      if ($user) {
        return "user login id already exist";
      }
    }


    foreach ($objUser->fillable as $key => $value) {
      if (!empty($request[$value]))
        $insertData[$value] = $request[$value];
    }
    if ($userType == 'school') {
      if ($request->hasFile('files')) {
        $objAttachments_Table = new Attachments_Table();
        $url = $objAttachments_Table->uploadAttechments($request);
        $url = $url[0];
        $insertData['logo_url'] = $url;
      }
    }

    if ($userType == 'teacher' || $userType == 'student') {
      if (!is_array($request) && $request->hasFile('image')) {
        $objAttachments_Table = new Attachments_Table();
        $url = $objAttachments_Table->uploadAttechments($request);
        $url = $url[0];
        $insertData['isProfilePic'] = '1';
        $insertData['profile_pic_url'] = $url;
      }
    }

    if (!empty($insertData)) {
      if (!empty($insertData["id"])) {
        $objUser->where(['id' => $insertData["id"]])->update($insertData->toArray());
      } else {
        $id = $objUser->create($insertData)->id;
      }

      if (!empty($request->teacherClassSection)) {
        (new Teacher_Class_Subject())->addUpdateTeacherClassSubject($id, $request->school_id, $request->teacherClassSection);
      } elseif (!empty($request['subject_class_id']) && is_array($request['subject_class_id'])) {
        (new Teacher_Class_Subject())->addUpdateTeacherClassSubject($id, $request['school_id'], $request['subject_class_id']);
      }

      (new Attachments_Table())->insertAttechment_new($request, $id, $objUser->getTable());
      $returndata["data"] = compact("id", 'login_id', 'password');
      return $returndata;
    }
  }

  public function string_encrypt($text)
  {
    $output         = false;
    $secret_key     = "ENUKEPASSCODE";
    $encrypt_method = "AES-256-CBC";
    $secret_iv      = 'g@@di';
    // hash
    $key = hash('sha256', $secret_key);
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    $output = openssl_encrypt($text, $encrypt_method, $key, 0, $iv);
    $output = base64_encode($output);
    return $output;
  }

  public function string_decrypt($text)
  {
    $output         = false;
    $secret_key     = "ENUKEPASSCODE";
    $encrypt_method = "AES-256-CBC";
    $secret_iv      = 'g@@di';
    // hash
    $key = hash('sha256', $secret_key);
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    $output = openssl_decrypt(base64_decode($text), $encrypt_method, $key, 0, $iv);
    return $output;
  }



  public function logoutUser($request, $userType)
  {
    $objUser = $this->getUserObject($userType);
    $user_id = User::getId($request);
    $data = $objUser->find($user_id);
    if ($data) {
      $data->token =null;
      $data->save();
    }
    return $data;
  }

  public function changePassword($request, $userType)
  {
    $objUser = $this->getUserObject($userType);
    $data = $objUser->find($request->user_id);
    $hashedPassword = $data->password;
    if (Hash::check($request->old_password, $hashedPassword)) {
      $data->password = Hash::make($request->new_password);
      $data->pass_code = $this->string_encrypt($request->new_password);
      $data->token = null;
      $data->save();
      return true;
    }
    return false;
  }
}

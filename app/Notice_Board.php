<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notice_Board extends Model
{
    protected $table = 'notice_board';
	
	protected $primaryKey = 'id';
	
	public $timestamps = false;

	public $fillable = ['id','school_id','title','message','type','date'];
	
	protected $hidden = [];	

    public function file_url()
    {
        return $this->hasMany('App\Attachments_Table','reference_id','id')->where('table_type',$this->table)->where('status',1);
    }

    public function getTable(){
    	return $this->table;
    }

	public function geNoticeForListing($request){
		$obj= self::select("notice_board.*","school.name as school_name")
         ->leftjoin('school','school.id','=','notice_board.school_id');
         if($request->school_id){
					$obj->where("school_id",$request->school_id);
         }
         return $obj->get();
	}


	public function getNotice($request){

			$obj = self::select($this->fillable);
			$fillable=$this->fillable;
			
						

			if($request->role != 'admin'){
				if($request->school_id){
					$obj->where('school_id',$request->school_id);
				}else if(session('user_school_id')!=''){
					$obj->where([ 'school_id'=>session('user_school_id')]);
				}
			}
			if($request->role == 'student')
			{
				$obj->where('type','!=','teacher');
			}
			else if($request->role == 'teacher')
			{
				$obj->where('type','!=','student');
			}

			if(!empty($request->search_string)){
				 $obj->where(function($query) use ($request ,$fillable){
					foreach ($fillable as $value) {
					$query->orWhere($value, 'LIKE', "%".$request->search_string."%");
					}
				});
			}
			if(!empty($request->page_order)){
				$obj->orderby($request->page_order[0],$request->page_order[1]);
			}
			if(!empty($request->page_limit)){
				$data = $obj->paginate($request->page_limit);
			}else{
				$data = $obj->get();
			}
			return $data;
		}


		public function addUpdateNotice($request){
		  
	      if(!empty($request->id)){
            $id = $request->id;
            $insertData = self::find($id);
	        }
            foreach ($this->fillable as $key => $value) {
	            if(!empty($request[$value]))
	                $insertData[$value]=$request[$value];
	        }
	        
	        if(!empty($request->id)){

	            self::where(['id'=>$id])->update($insertData->toArray());
	            (new Notifaction())->setNewNotificatin($request,$this->getTable(),$id,'update');
	         }
	         else{
         		$id = self::create($insertData)->id;
     			(new Notifaction())->setNewNotificatin($request,$this->getTable(),$id,'add');
	         }
	        return compact("id"); 
		}

}

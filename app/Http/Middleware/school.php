<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class school
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
   
    public function handle($request, Closure $next)
    {
         $role= $request->route()->getPrefix();
        if(!Auth::guard($role)->check()){
            return redirect('/'.$role);
        }
        return $next($request);

        // if(!Auth::guard('school')->check()){
        //     return redirect('/admin');
        // }
        // return $next($request);
    }
}

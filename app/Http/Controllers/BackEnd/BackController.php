<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Session_Table;
use App\User,App\Student,App\Teacher,App\School,App\Notice_Board;
use Session;
use Auth;
use Hash;
use DB;

class BackController extends Controller
{
    public function __construct()
    {
        view()->share('page_title', 'DASHBOARD');
    }


    public function index(Request $request){
        $role= USER::getPrifix($request);
        $school_name = last($role);
        $role = $role[0];

        if(!empty(session("user_details"))){
            return redirect($role.'/dashboard');
        }
        if($request->isMethod('post')){
             $rules = [
                'login_id' => 'required',
                'password' => 'required|min:3',
                'role' => 'required|in:student,teacher,admin,user,school',
            ];
            if(!empty($request->role) && !in_array($request->role, ['admin','school'])){
                $rules['school_id'] ='required|numeric';
            }
            $validatedData = $request->validate($rules );
            $objUser= new  User();
            $data = $objUser->getUserLogin($request, $request->role);
            if($data)
            {   
                return redirect($request->role.'/dashboard');
            }
            else
            {
                Session::flash('error_message','Invalid Email or Password');
                return redirect()->back();
            }
        }
        return view('BackEnd.login',compact('role','school_name'));
    }
    
    public function dashboard(Request $request){

        $role= USER::getPrifix($request);
        $school_name=last($role);
        $request->role = $role=$role[0];
        $studentCount=(new Student())->getStudentList()->count();
        $teacherCount=(new Teacher())->getTeacherList()->count();
        $SchoolCount = (new School())->getSchoolList()->count();
        $NoticeCount = (new Notice_Board())->getNotice($request)->count();
        
        $obj = DB::table('session_table')->select('session_table.id','session_table.date','session_table.online_class_url','session_table.start_time','session_table.end_time','session_table.meeting_id','session_table.password','session_table.content','class_section.class_name','class_section.section_name','subject_master.subject_name', 'teacher.name as teacher_name');
			$todayDate=date('Y-m-d');
            if(session('user_school_id')!='')
    			$obj->where([ 'session_table.school_id'=>session('user_school_id')]);
            $obj->Join('teacher_class_subject','teacher_class_subject.id','=','session_table.teacher_class_subject_id')
	        ->Join('subject_class','subject_class.id','=','teacher_class_subject.subject_class_id')
	        ->Join('subject_master','subject_master.id','=','subject_class.subject_id')
	        ->Join('class_section','class_section.id','=','subject_class.class_section_id')
	        ->join('teacher',["teacher.id"=>"teacher_class_subject.teacher_id"]);
	        $obj->where('session_table.date','>=', $todayDate);
            $obj->orderby('session_table.date',"ASC");

            $obj->groupby('session_table.id');
           $UpcommingSession = $obj->get();
        return view('BackEnd.dashboard',compact("SchoolCount","studentCount","teacherCount","UpcommingSession","role","NoticeCount"));
    }
    public function logout(Request $request){
        Session::flush();
        $role= $request->route()->getPrefix();
        Auth::guard($role)->logout();
        return redirect("/$role");
    }
}

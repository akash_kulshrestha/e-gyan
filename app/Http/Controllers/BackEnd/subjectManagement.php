<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Subject_Master;
use Auth;
use DB;
use Session;

class subjectManagement extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        view()->share('page_title', 'SUBJECT MASTER');
    }
    public function index()
    {
        $subjects=DB::table('subject_master')->get();
        return view('BackEnd/subjectManagement.subject',compact("subjects"));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check = DB::table('subject_class')
        ->where('subject_id', '=', $id)
        ->get();
        if (empty($check[0]->id)) {
            DB::table('subject_master')->where('id','=',$id)->delete();
            Session::flash('success_message','Subject deleted successfully');
        } else {
            Session::flash('error_message',"You can't delete this subject because this is available in subject mapping.");
        } 
        return redirect(session("role").'/subject');
    }
}

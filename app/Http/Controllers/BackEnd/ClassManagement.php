<?php
namespace App\Http\Controllers\BackEnd;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use Session;
use Hash;
use Importer;
use App\Class_Section;
use \Validator;
use Redirect;

class ClassManagement extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        view()->share('page_title', 'CLASS MANAGEMENT');
    }

    public function index()
    {
        

         $classes =(new Class_Section())->getClassSectionList();
         return view('BackEnd/classManagement.classes',compact("classes"));
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check = DB::table('subject_class')
        ->where('class_section_id', '=', $id)
        ->get();
        if (empty($check[0]->id)) {
            DB::table('class_section')->where('id','=',$id)->delete();
            Session::flash('success_message','Class deleted successfully');
            return redirect(session("role").'/class');
        } else {
            Session::flash('error_message',"You can't delete this class-section because this is available in subject mapping.");
            return redirect(session("role").'/class');
        }
      
    }
}

<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use \Validator;
use Auth;
use User;
use Illuminate\Validation\Rule;
use App\Session_Table;


class SessionController extends \App\Http\Controllers\Controller
{
    public function deleteSession(Request $request)
    {
        try {
            $rules = [
                'id'=>'required|numeric',
                'school_id' => 'required|numeric',
                'role'=>'required|in:student,teacher,admin,user,school'
            ];
            if($request->role != 'super_admin')
            {
                $validatedData = Validator::make( $request->all(),$rules);
                if ($validatedData->fails()){          
                    return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
                }
            }            

            if($id)
                return $this->apiResponse(['message'=>'Session deleted','id'=>$id]);
            else
                return $this->apiResponse([]);;

            
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function addSession(Request $request)
    {
        try {
            $rules = [
                //'date'=>'required|date',
                'teacher_class_subject_id'=>'required|numeric',
                'start_time'=>'required|date',
                'end_time'=>'required|date'
            ];
            if(!empty($request->meeting_id))
            {
                $rules['meeting_id'] = 'required|unique:session_table';
            }
            $validatedData = Validator::make( $request->all(),$rules);

            
            if ($validatedData->fails()){          
                return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }
            $objSession_Table= new  Session_Table();
            $data= $objSession_Table->addUpdateSession($request);

            return $this->apiResponse(['message'=>'Session added','data'=>$data]);
            
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }


    public function updateSession(Request $request)
    {
        try {
            $rules = [
                    'session_id'=>'required|numeric'
            ];
            if(!empty($request->meeting_id))
            {
                $rules['meeting_id'] = [
                        'required',
                        Rule::unique('session_table')->ignore($request->session_id)
                    ];
            }
            $validatedData = Validator::make( $request->all(),$rules);
            if ($validatedData->fails()){          
                return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }
            $data = Session_Table::find($request->session_id);
            if(!$data)
                return $this->apiResponse([]);
            
            $objSession_Table= new  Session_Table();
            $data= $objSession_Table->addUpdateSession($request);
            return $this->apiResponse(['message'=>'Session Updated','data'=>$data]);
            
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }



    public function getUpcomingClassSessions(Request $request)
    {
        try {
            $rules['role']='required|in:student,teacher,admin,user,school';
            $validatedData = Validator::make( $request->all(),$rules);
        
            if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }
            
            $objSession_Table= new  Session_Table();
            $data= $objSession_Table->getTodaysSession($request);
           
            if($data){
                if(!empty($request->page_limit)){
                    return $this->apiResponse($data);
                }
                return $this->apiResponse(['data'=>$data]);
            }
            else
                return $this->apiResponse(["data"=>[]]);
  
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }
    


}

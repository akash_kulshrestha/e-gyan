<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use \Validator;
use Auth;
use DB;

use App\Student, App\User ,App\Subject_Class, App\School;
use App\Notifaction;

class StudentController extends \App\Http\Controllers\Controller
{
     public function getListPage(Request $request)
    {
       $Student=  new Student();
        $response = $Student->getListPage($request);
        return view('BackEnd/studentManagement.student',compact("response",'request'));
    }

    public function getForm(Request $request){
     
        $Student =new Student();
        $data = $Student->getStudentDetails($request);
        $schools = School::select('id',"name")->get();
        $classSections = DB::table('class_section')->get();
        $request->optional=1;
        $subject_optional = (new Subject_Class())->getList($request);
        return view('BackEnd/studentManagement.editStudent',compact("data","schools","classSections","subject_optional"));
    }

    public function addupdateStudents(Request $request)
    {
         try {
            $objUser= new  Student();
            $rules= $objUser->getsignUpRules($request);

            $validatedData = Validator::make( $request->all(),$rules);
            if ($validatedData->fails()){          
                return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }

            $data = $objUser->createUpdate($request);

            if(!empty($data['data'])){
                $data['message']='Successfully Updated';
                return $this->apiResponse($data);   
            }
            else
                return $this->apiResponse(['error'=>$data],true);

        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }


  

}

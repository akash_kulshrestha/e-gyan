<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use \Validator;
use Auth;
use App\Session_Table, App\Student, App\Teacher, App\Assignment, App\Attachments_Table;
use App\Assignment_Submittted;

use \DB;
use Carbon\Carbon;


class AssignmentController extends \App\Http\Controllers\Controller
{
    public function deleteAssignment(Request $request)
    {
        try {

            $rules = [
                    'id'=>'required|numeric',
                    'school_id' => 'required|numeric',
                    'role'=>'required|in:student,teacher,admin,user,school'
            ];
            if($request->role != 'super_admin')
            {
                $validatedData = Validator::make( $request->all(),$rules);
                if ($validatedData->fails()){          
                     return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
                }
            }

            $objAssignment= new Assignment();
            // $id = $objAssignment->;
            
            if($id)
                return $this->apiResponse(['message'=>'Assignment deleted','id'=>$id]);
            else
                return $this->apiResponse([]);;

        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function deleteAssignmentSubmittted(Request $request)
    {
        try {

            $rules = [
                    'id'=>'required|numeric',
                    'school_id' => 'required|numeric',
                    'role'=>'required|in:student,teacher,admin,user,school'
            ];
            if($request->role != 'super_admin')
            {
                $validatedData = Validator::make( $request->all(),$rules);
                if ($validatedData->fails()){          
                     return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
                }
            }
        
            $objAssignment= new Assignment_Submittted();
            // $id = $objAssignment->;
            
            if($id)
                return $this->apiResponse(['message'=>'Submittted Assignment deleted','id'=>$id]);
            else
                return $this->apiResponse([]);;

        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function addAssignment(Request $request)
    {
        try {

            $rules = [
                    'school_id' => 'required|numeric',
                    'role'=>'required|in:student,teacher,admin,user,school',
                    'teacher_class_subject_id' => 'required',
                    'title'=>'required|max:255',
                    'assignment_description'=>'required',
                    'due_date'=>'required|date'
            ];
            $validatedData = Validator::make( $request->all(),$rules);
           
            if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }

            $objAssignment= new Assignment();
            $id = $objAssignment->addUpdateAssignmentbyTeacher($request);
            
            return $this->apiResponse(['message'=>'Assignment added','id'=>$id]);
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function submitAssignmentRemark(Request $request){
        try {

            $rules = [
                    'school_id' => 'required|numeric',
                    "assignment_id"=>'required|numeric',
                    "student_id"=>'required|numeric',
                    "teacher_status"=>'required|in:Accepted,Rejected',
                    "grade" =>'required',
                    'role'=>'required|in:teacher,admin,user,school',
                    'teacher_class_subject_id' => 'required'
            ];
            $validatedData = Validator::make( $request->all(),$rules);
           
            if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }

            $objAssignment_Submittted= new Assignment_Submittted();
            $data= $objAssignment_Submittted->submitAssignmentRemark($request);
            
            return $this->apiResponse(['data'=>$data]);
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function studentlistForAssignment(Request $request)
    {
        try {

            $rules = [
                    'school_id' => 'required|numeric',
                    'role'=>'required|in:student,teacher,admin,user,school',
                    'teacher_class_subject_id' => 'required'
            ];
            $validatedData = Validator::make( $request->all(),$rules);
           
            if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }

            $objStudent= new Student();
            $data= $objStudent->getAllStudentList(['school_id'=>$request->school_id,'teacher_class_subject_id'=>$request->teacher_class_subject_id]);
            
            return $this->apiResponse(['data'=>$data]);
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function submitAssignmentByStudent(Request $request)
    {
        try {
            $rules = [
                    'school_id' => 'required|numeric',
                    'assignment_id' => 'required|numeric',
                    'details'=>'required'
            ];
            $validatedData = Validator::make( $request->all(),$rules);
           
            if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }
           
            $objAssignment_Submittted= new Assignment_Submittted();
            $id=$objAssignment_Submittted->submitAssignment($request);

            return $this->apiResponse(['message'=>'Assignment added','id'=>$id]);
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function getAssignmentList(Request $request)
    {
        try {
            $rules=[
                    'school_id' => 'required|numeric',
                    'role' => 'required|in:student,teacher,user'
            ];
            
            $validatedData = Validator::make( $request->all(),$rules);

            if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }
            
            $objAssignment= new Assignment();
            $data= $objAssignment->getAssignment($request);

                if(!empty($request->page_limit)){
                    
                    return $this->apiResponse($data);
                }
                return $this->apiResponse(['data'=>$data]);
           

        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function getAssignmentById($id, Request $request)
    {
        try {
            
            $rules=[
                    'school_id' => 'required|numeric',
                    'role' => 'required|in:student,teacher,user'
                ];
            if(!empty($request->role) && $request->role=='teacher'){
               
            }else{
                $rules['class_section_id']='required';
                $rules['subject_ids']='required';
            }
            $validatedData = Validator::make( $request->all(),$rules);

            if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }
            
            $objAssignment= new Assignment();
            $data= $objAssignment->getAssignment($request,$id);

            // if($data){
                if(!empty($request->page_limit)){
                    
                    return $this->apiResponse($data);
                }
                return $this->apiResponse(['data'=>$data]);
            // }
            // else
            //     return $this->apiResponse([]);;

        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }


 public function getMyCreatedAssingment(Request $request){
    try {
            $rules=[
                'school_id'=> 'required|numeric',
                'role' => 'required|in:teacher'
            ];
            $validatedData = Validator::make( $request->all(),$rules);
            if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }
            $objAssignment= new Assignment();
            $data= $objAssignment->getMyCreatedAssingment($request);
            if($data){
                if(!empty($request->page_limit)){
                    
                    return $this->apiResponse($data);
                }
                return $this->apiResponse($data);
            }
            else
                return $this->apiResponse([]);;

        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
 }   
 public function getAssingmentSubmittedStudentList(Request $request){
    try {
            $rules=[
                'school_id'                 => 'required|numeric',
                "teacher_class_subject_id"  => 'required|numeric',
                'role' => 'required|in:teacher',
                'assignment_id'=>'required|numeric'
            ];
            $validatedData = Validator::make( $request->all(),$rules);
            if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }
            $objAssignment= new Assignment();
            $data= $objAssignment->getAssingmentSubmittedStudentList($request);
            if($data){
                // if(!empty($request->page_limit)){
                //     
                //     return $this->apiResponse($data);
                // }
                return $this->apiResponse(['data'=>$data]);
            }
            else
                return $this->apiResponse([]);;

        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }

 }   
 public function getsubmittedAssigmentStudentDetail(Request $request){
    try{
            $rules=[
                'school_id'                 => 'required|numeric',
                "teacher_class_subject_id"  => 'required|numeric',
                'role'                      => 'required|in:teacher',
                'assignment_submittted_id'  => 'required|numeric'
            ];
            $validatedData = Validator::make( $request->all(),$rules);
            if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }
            $objAssignment= new Assignment();
            $data= $objAssignment->getsubmittedAssigmentStudentDetail($request);
            if($data){
                if(!empty($request->page_limit)){
                    
                    return $this->apiResponse($data);
                }
                return $this->apiResponse(['data'=>$data]);
            }
            else
                return $this->apiResponse([]);;

        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
 }   

 
 public function checkAssignmentStatus(Request $request)
 {
     try {
         $rules=[
                 'school_id' => 'required|numeric',
                 'role' => 'required|in:student',
                 "assignment_id"=>'required|numeric'
             ];
       
         $validatedData = Validator::make( $request->all(),$rules);

         if ($validatedData->fails()){          
              return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
         }
         
         $objAssignment= new Assignment_Submittted();
         $data= $objAssignment->checkAssignmentStatus($request);

         if($data){
              if(!empty($request->page_limit)){
                 
                 return $this->apiResponse($data);
             }
             return $this->apiResponse(['data'=>$data]);
         }
         else
             return $this->apiResponse([]);;

     } catch(\Exception $e) {
         return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
     }
 }


 public function getSubmittedAssignmentList(Request $request)
    {
        try {
            $rules=[
                    'school_id' => 'required|numeric',
                    'role' => 'required|in:student,teacher,user',
                    "assignment_id"=>'required|numeric'
                ];
          
            $validatedData = Validator::make( $request->all(),$rules);

            if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }
            
            $objAssignment= new Assignment_Submittted();
            $data= $objAssignment->getSubmittedAssignment($request);

            if($data){
                 if(!empty($request->page_limit)){
                    
                    return $this->apiResponse($data);
                }
                return $this->apiResponse(['data'=>$data]);
            }
            else
                return $this->apiResponse([]);;

        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    

}

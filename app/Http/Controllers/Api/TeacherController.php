<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use \Validator;
use Auth;
use DB;

use App\Teacher, App\Teacher_Class_Subject ,App\Subject_Class, App\School ,App\Class_Section;
use App\Notifaction;

class TeacherController extends \App\Http\Controllers\Controller
{
     public function getListPage(Request $request)
    {
        $Teacher=  new Teacher();
        $response = $Teacher->getTeacherList($request);
        foreach($response as $key=>$value){
            $teacher_class_subject = Teacher_Class_Subject::select(DB::raw('GROUP_CONCAT(CONCAT_WS("-",class_name,section_name,subject_name)) as class_section_subject'))
            ->leftJoin('subject_class','subject_class.id','=','teacher_class_subject.subject_class_id')
            ->leftJoin('subject_master','subject_master.id','=','subject_class.subject_id')
            ->leftJoin('class_section','class_section.id','=','subject_class.class_section_id')
            ->where('teacher_class_subject.teacher_id',$value->id)
            ->where('teacher_class_subject.status',1);
            if(session('user_school_id')!=''){
                $teacher_class_subject= $teacher_class_subject->where([ 'teacher_class_subject.school_id'=>session('user_school_id')]);
            }
            $teacher_class_subject= $teacher_class_subject->groupby('teacher_class_subject.teacher_id')
            ->first();
            if(empty($teacher_class_subject)){
                $response[$key]->teacher_class_subject ='';
                continue;
            }
            $teacher_class_subject=$teacher_class_subject->toArray();
          
            $response[$key]->teacher_class_subject = $teacher_class_subject['class_section_subject'];
        }
        return view('BackEnd/teacherManagement.teacher',compact("response",'request'));
    }

    public function getForm(Request $request){
        $slectedClassSubjects=[];
        $Teacher =new Teacher();
        $data = $Teacher->getTeacherDetails($request);
        $schools = School::select('id',"name")->get();
        if($request->id > 0){
            $slectedClassSubjects=DB::table('teacher_class_subject')->where(['teacher_id'=>$request->id,'status'=>1])->get();
            $slectedClassSubjects = $slectedClassSubjects->pluck('subject_class_id')->toArray(); 
        }
        $usedids=(new Teacher_Class_Subject())->checkClassSubject($request->id, $request->school_id,[]);
        $classSections = (new Class_Section())->getunSelectedsectionNameWithsubject($usedids);
        return view('BackEnd/teacherManagement.editTeacher',compact("data","schools","classSections","slectedClassSubjects","request"));
    }

    public function addupdateTeachers(Request $request)
    {
         try {
            $objUser= new  Teacher();
            $rules= $objUser->getsignUpRules($request);

            $validatedData = Validator::make( $request->all(),$rules);
            if ($validatedData->fails()){          
                return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }
            $data=[];
            if(!empty($request->teacherClassSection)){
              $data=  (new Teacher_Class_Subject())->checkClassSubject($request->id, $request->school_id, $request->teacherClassSection); 
              }elseif(!empty($request['subject_class_id']) && is_array($request['subject_class_id'])){
               $data= (new Teacher_Class_Subject())->checkClassSubject($request->id, $request['school_id'], $request['subject_class_id']); 
              }
              if(count($data)>0){
                return $this->apiResponse(['error' => "Class-Section already given to another teacher" ,'data'=>$data, 'message'=> "Class-Section already given to another teacher"], true);
              }
            $data = $objUser->createUpdate($request);

            if(!empty($data['data'])){
                $data['message']='Successfully Updated';
                return $this->apiResponse($data);   
            }
            else
                return $this->apiResponse(['error'=>$data],true);

        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }


  

}

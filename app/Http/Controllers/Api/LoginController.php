<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use \Validator;
use Auth;
use DB;
use Hash;
use  App\Student, App\Teacher, App\User, App\Subject_Class, App\Class_Section, App\Teacher_Class_Subject;


class LoginController extends \App\Http\Controllers\Controller
{
	
	public function logout(Request $request){
		try{
		    $objUser= new  User();
            $data = $objUser->logoutUser($request, $request->role);
        	if($data)
        	{   
		 		return $this->apiResponse(['message' => 'successfull logout']);
        	}
        	else
        	{
        		return $this->apiResponse(['error' => 'Logout not successful'],true);	
        	}
		}
		catch(\Exception $e) {
			return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
		}
	}

    public function getUserLogin(Request $request){
        $objUser= new  User();
        // $Attachments_Table = DB::table('attachments_table')->where(['reference_id'=>$request->user_id,'table_type'=>$request->role,'status'=>1])->first();
        // $data=  [
        //     "role"=> $request->role,
        //     "school_id"=> $request->school_id,
        //     "user_id"=> $request->user_id,
        //     "file_url"=>$Attachments_Table];

        $returnData =  $objUser->getUserAllDetails($request->user_id, $request->role);
        $returnData['role'] = $request->role;
        unset($returnData['password'], $returnData['pass_code'],$returnData['token'],$returnData['school']['token']);
        return $this->apiResponse(['data'=> $returnData]);
    }

    public function getschool(Request $request){
            try {
            
            $rules = [
                'login_id' => 'required',
                 'role' => 'required|in:student,teacher,admin,user,school',
            ];

            $validatedData = Validator::make( $request->all(),$rules);
           
            if ($validatedData->fails()){          
                return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }

            $objUser= new  User();
            $data = $objUser->getschoolinfo($request, $request->role);

            if($data['user']){
                return $this->apiResponse(['message' => 'Successfully','data'=>$data]);
            }
            else
                return $this->apiResponse([]);
        
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }

    }

    public function getNotification(Request $request){
        $data= DB::table('web_notification')->where(['user_role'=>$request->role,'user_id'=>$request->user_id])->orderBy('id','DESC')->get();
        if($data){
            $data=$data->toArray();
           DB::table('web_notification')->where(['user_role'=>$request->role,'user_id'=>$request->user_id])->update(['status'=>1]);
            return $this->apiResponse(['data'=>$data]);
        }
        return $this->apiResponse([]);
    }

    public function login(Request $request)
    {
        try {
            $rules = [
                'login_id' => 'required',
                'password' => 'required|min:3',
                'role' => 'required|in:student,teacher,admin,user,school',
            ];

            $validatedData = Validator::make( $request->all(),$rules);
           
            if ($validatedData->fails()){          
                return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }

            $objUser= new  User();
            $data = $objUser->getUserLogin($request, $request->role);
            if($data){
                return $this->apiResponse(['message' => 'Successfully login','data'=>$data]);
            }
            else
                return $this->apiResponse(['error'=>'Invalid login credentials'],true,401);
        
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true,500);
        }
    }


    public function signup(Request $request)
    {
        try {

            $objUser= new  User();
            $rules= $objUser->getsignUpRules($request);
            $customMessages = [
                'regex' => 'Password atleast have one digit ,one capital one small character.'
            ];
             $validatedData = Validator::make( $request->all(),$rules,$customMessages);
            if ($validatedData->fails()){          
                return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }
          
            $objUser= new  User();
            $data = $objUser->createUpdateUser($request, $request->role);
            if(!empty($data['data'])){
                $data['message']='Successfully Created';
                return $this->apiResponse($data);
            }
            else
                return $this->apiResponse(['error'=>$data, 'message'=>$data],true);

        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    
    public function changePassword(Request $request) 
    {
        try
        {
            $rules = [
                'old_password' => 'required|min:3',
                'new_password' => 'required|min:3|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/'
             ]; 
             $customMessages = [
                'regex' => 'Password atleast have one digit ,one capital one small character.'
            ];
             $validatedData = Validator::make( $request->all(),$rules,$customMessages);
            if ($validatedData->fails()){          
                return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }

            $objUser= new  User();
            $check = $objUser->changePassword($request, $request->role);
            if($check){
                 return $this->apiResponse(['message'=>'Password Changed Successfully']);
            }
            else
                return $this->apiResponse(['message'=>'Password Not Match with Old Password'],true);
        
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }


	public function useredit(Request $request)
	{
		try {
			$objUser= new  User();
			$rules= $objUser->getsignUpRules($request);

		    $validatedData = Validator::make( $request->all(),$rules);
            if ($validatedData->fails()){          
                return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }

        
            $objUser= new  User();
            $data = $objUser->createUpdateUser($request, User::getRequestRole());

            if(!empty($data['data'])){
            	$data['message']='Successfully Updated';
            	return $this->apiResponse($data); 	
            }
            else
                return $this->apiResponse(['error'=>$data],true);

		} catch(\Exception $e) {
			return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
		}
    }

    public function toDelete(Request $request){
        $data= DB::table($request->table)->where("id",$request->id)->first();
        if($data){
            DB::table($request->table)->where("id",$request->id)->delete();
            return $this->apiResponse(["message"=>"delete successfully"]);   
        }
        else
         return $this->apiResponse(["message"=>"No record found"]);   
       
    }   


}

<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use \Validator;
use Auth;
use App\App_Version, App\Quiz_Table, App\Quiz_Detail ,App\Quiz_Result;
use DB;


class QuizController extends \App\Http\Controllers\Controller
{
    public function addUpdateQuizTable(Request $request)
    {
        try {
            $rules = [
                    'class_section_id' => 'required|numeric',
                    'subject_id' => 'required|numeric',
                    'total_time' => 'required',
                    'role' => 'required|in:student,teacher,admin,user,school'
            ];
            if($request->role != 'super_admin')
            {
                $validatedData = Validator::make( $request->all(),$rules);
                if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
                }
            }

            $objQuiz_Table = new Quiz_Table();
            $data= $objQuiz_Table->inserUpdateData($request);
            if(!empty($data['id'])){
                if(!empty($request->id)){
                    $data['message']='Successfully Updated';
                    return $this->apiResponse($data);
                }
                else
                {
                    $data['message']='Successfully Created';
                    return $this->apiResponse($data);                    
                }
            }
            else
                return $this->apiResponse(['error'=>$data, 'message'=>$data],true);            
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function addUpdateQuizDetail(Request $request)
    {
        try {
            $rules = [
                    'quiz_table_id' => 'required|numeric',
                    'question_number' => 'required|numeric',
                    'option_A' => 'required',
                    'option_B' => 'required',
                    'option_C' => 'required',
                    'option_D' => 'required',
                    'correct_answer' => 'required',
                    'role' => 'required|in:student,teacher,admin,user,school'
            ];
            if($request->role != 'super_admin')
            {
                $validatedData = Validator::make( $request->all(),$rules);
                if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
                }
            }
            $objQuiz_Detail = new Quiz_Detail();
            $data= $objQuiz_Detail->inserUpdateData($request);
            if(!empty($data['id'])){
                if(!empty($request->id)){
                    $data['message']='Successfully Updated';
                    return $this->apiResponse($data);
                }
                else
                {
                    $data['message']='Successfully Created';
                    return $this->apiResponse($data);                    
                }
            }
            else
                return $this->apiResponse(['error'=>$data, 'message'=>$data],true);            
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function addUpdateQuizResult(Request $request)
    {
        try {
            $rules = [
                    'user_id' => 'required',
                    'school_id' => 'required',
                    'total_question' => 'required',
                    'attempt_count' => 'required',
                    'non_attempt_count' => 'required',
                    'correct_count' => 'required',
                    'wrong_count' => 'required',
                    'percentage' => 'required',
                    'role' => 'required|in:student,teacher,admin,user,school',
                    'quiz_table_id' => 'required|numeric',
            ];
            if($request->role != 'super_admin')
            {
                $validatedData = Validator::make( $request->all(),$rules);
                if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
                }
            }
            $objQuiz_Result = new Quiz_Result();
            $data= $objQuiz_Result->inserUpdateData($request);
            if(!empty($data['id'])){
                if(!empty($request->id)){
                    $data['message']='Successfully Updated';
                    return $this->apiResponse($data);
                }
                else
                {
                    $data['message']='Successfully Created';
                    return $this->apiResponse($data);                    
                }
            }
            else
                return $this->apiResponse(['error'=>$data, 'message'=>$data],true);            
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function getTableQuiz(Request $request)
    {
        try {
            $rules = [
                    'role' => 'required|in:student,teacher,admin,user,school',
                    'class_section_id' => 'required|numeric',
                    'subject_id' => 'required|numeric'
            ];
            if($request->role != 'super_admin')
            {
                $validatedData = Validator::make( $request->all(),$rules);
                if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
                }
            }
            $objQuiz_Table = new Quiz_Table();
            $data = $objQuiz_Table->getTableData($request);
            if(!empty($data)){
                return $this->apiResponse(['data'=>$data]);
            }
            else
                return $this->apiResponse([]);;            
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function getQuiz(Request $request)
    {
        try {
            $rules = [
                    'role' => 'required|in:student,teacher,admin,user,school',
                    'quiz_table_id' => 'required|numeric'
            ];
            if($request->role != 'super_admin')
            {
                $validatedData = Validator::make( $request->all(),$rules);
                if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
                }
            }
            $objQuiz_Detail = new Quiz_Detail();
            $data = $objQuiz_Detail->getQuiz($request);
            if(!empty($data)){
                return $this->apiResponse(['data'=>$data]);
            }
            else
                return $this->apiResponse([]);;            
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function getQuizResult(Request $request)
    {
        try {
            $rules = [
                    'role' => 'required|in:student,teacher,admin,user,school',
                    'quiz_table_id' => 'required|numeric'
            ];
            if($request->role != 'super_admin')
            {
                $validatedData = Validator::make( $request->all(),$rules);
                if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
                }
            }
            $objQuiz_Result = new Quiz_Result();
            $data = $objQuiz_Result->getQuizResult($request);
            if(!empty($data)){
                return $this->apiResponse(['data'=>$data]);
            }
            else
                return $this->apiResponse([]);;            
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

}

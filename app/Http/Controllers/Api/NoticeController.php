<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use \Validator;
use Auth;
use App\Notice_Board;
use App\School;


class NoticeController extends \App\Http\Controllers\Controller
{
    public function deleteNotice(Request $request)
    {
        try {
            $rules = [
                'id'=>'required|numeric',
                'school_id' => 'required|numeric',
                'role'=>'required|in:student,teacher,admin,user,school'
            ];

            if($request->role != 'super_admin')
            {
                $validatedData = Validator::make( $request->all(),$rules);
                if ($validatedData->fails()){          
                     return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
                }
            }
            
            $objNotice_Board = new Notice_Board();
            // $id= $objNotice_Board->;

            if($id)
                return $this->apiResponse(['message'=>'Notice deleted','id'=>$id]);
            else
                return $this->apiResponse([]);;
            
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function updateNotice(Request $request)
    {
        try {
            $rules = [
                'id'=>'required|numeric',
                'school_id' => 'required|numeric',
                'role' => 'required|in:student,teacher,user,admin,school',
                'type'=>'required|in:teacher,student,all'
            ];
            
            if($request->role != 'super_admin')
            {
                $validatedData = Validator::make( $request->all(),$rules);
                if ($validatedData->fails()){          
                     return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
                }      
            }
            $objNotice_Board = new Notice_Board();
            $id= $objNotice_Board->addUpdateNotice($request);
            return $this->apiResponse(['message'=>'Notice Updated','id'=>$id]);
            
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function addNotice(Request $request)
    {
        try {
            $rules = [
                'school_id' => 'required|numeric',
                'role' => 'required|in:student,teacher,user,school,admin',
                'title'=> 'required|max:255',
                'message'=>'required',
                'type'=>'required|in:teacher,student,all'
            ];
            
            if($request->role != 'super_admin')
            {
                $validatedData = Validator::make( $request->all(),$rules);
                if ($validatedData->fails()){          
                     return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
                }      
            }
            $objNotice_Board = new Notice_Board();
            $id= $objNotice_Board->addUpdateNotice($request);

            return $this->apiResponse(['message'=>'Notice added','id'=>$id]);

            
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function addupdatenotice(Request $request){
       // try {
            $rules = [
                'title'=> 'required|max:255',
                'message'=>'required',
                'type'=>'required|in:teacher,student,all'
            ];
            
            $validatedData = Validator::make( $request->all(),$rules);
            if ($validatedData->fails()){          
                return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }         
            $objNotice_Board = new Notice_Board();
            $id= $objNotice_Board->addUpdateNotice($request);
            return $this->apiResponse(['message'=>'Notice added','id'=>$id]);
            
        // } catch(\Exception $e) {
        //     return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        // }
    }

	public function noticeList(Request $request)
	{
		try {
            $objNotice_Board = new Notice_Board();
            $data= $objNotice_Board->getNotice($request);

            if($data){
                if(!empty($request->page_limit)){
                    
                    return $this->apiResponse($data);
                }
                return $this->apiResponse(['data'=>$data]);
            }
            else
                return $this->apiResponse([]);;
		    
		} catch(\Exception $e) {
			return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
		}
	}





    public function noticeForm(Request $request){
                                  
        $type =['all','teacher','student'];    
        $objSchool = new School();
        $schools = $objSchool->getAlldata();
        $notice=[];

        if(!empty($request->id)){
           $notice = (new Notice_Board())->with('file_url')->find($request->id)->toArray();
        }

        $request=$request->all();
        return view('BackEnd/noticeManagement.addeditnotice',compact("schools","type","notice","request"));  
    }
	
    public function makeNoticeList(Request $request){
        $data = $request->all();
        return view('component/noticelist',compact("data"));  
    }

    public function getListPage(Request $request){
        $response= (new Notice_Board())->geNoticeForListing($request);
        // if(session('user_school_id')!='')
        //     $response->where('notice_board.school_id',session('user_school_id'));
        // $response = $notices->get();    
        // ); 
        return view('BackEnd/noticeManagement.notice',compact("response",'request'));
    }
}
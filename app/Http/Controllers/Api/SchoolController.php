<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use \Validator;
use Auth;
use App\Student, App\User ,App\Subject_Class, App\School;
use App\ReadMapping;
use App\Notifaction;

class SchoolController extends \App\Http\Controllers\Controller
{
    public function getSchoolListPage(Request $request)
    {
        try 
        {
            $School = new School();
            $response = $School->getSchoolList($request);
            return view('BackEnd/schoolManagement/school',compact("response","request"));  
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function getSchoolForm(Request $request){
        $data= null;
        if(!empty($request->id)){
            $objUser= new  School();
            $data=$objUser->getAllDetails($request->id);
        }
        return view('BackEnd/schoolManagement.addeditSchool',compact('data',"request"));
    }


    public function SchoolAddEdit(Request $request){
        try {
            $objUser= new  School();
            $rules= $objUser->getsignUpRules($request);

            $validatedData = Validator::make( $request->all(),$rules);
            if ($validatedData->fails()){          
                return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }

            $data = $objUser->createUpdate($request);

            if(!empty($data['data'])){
                $data['message']='Successfully Updated';
                return $this->apiResponse($data);   
            }
            else
                return $this->apiResponse(['error'=>$data],true);

        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }


    


}

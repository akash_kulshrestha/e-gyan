<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use \Validator;
use Auth;
use App\Subject_Class, App\Subject_Master;
use DB;


class SubjectMasterController extends \App\Http\Controllers\Controller
{

    public function getListPage(Request $request)
    {
        $Subject_Master= new Subject_Master();
        $response= $Subject_Master->getList($request);
        return view('BackEnd/subjectManagement.list',compact("response",'request'));
    }

    public function getForm(Request $request){
     
        $schools = DB::table('school')->get();
        $subject_master=[];
        if(!empty($request->id)){
           $subject_master = Subject_Master::find($request->id);
        }
        $request=$request->all();
        return view('BackEnd/subjectManagement.addeditmasterform',compact("schools",'subject_master','request'));  
    }

    public function addUpdate(Request $request)
    {
        try {
            $rules = [
                    'school_id' => 'required|numeric',
                    'role'=>'required|in:student,teacher,admin,user,school',
                    'app_version'=>'required',
                    'mandatory_status'=>'required|in:low,high,medium'
            ];
           
            $validatedData = Validator::make( $request->all(),$rules);
            if ($validatedData->fails()){          
             return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }

            $objApp_Version = new App_Version();
            $data= $objApp_Version->inserUpdateData($request);
            if(!empty($data['id'])){
                if(!empty($request->id)){
                    $data['message']='Successfully Updated';
                    return $this->apiResponse($data);
                }
                else
                {
                    $data['message']='Successfully Created';
                    return $this->apiResponse($data);                    
                }
            }
            else
                return $this->apiResponse(['error'=>$data, 'message'=>$data],true);            
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }


    public function deleteSubject(Request $request)
    {
        try {
            $rules = [
                    'id'=>'required|numeric',
                    'school_id' => 'required|numeric',
                    'role'=>'required|in:student,teacher,admin,user,school'
            ];
            
            if($request->role != 'super_admin')
            {
                $validatedData = Validator::make( $request->all(),$rules);
                if ($validatedData->fails()){          
                     return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
                }
            }
            
            $objSubject_Master = new Subject_Master();
            // $id = $objSubject_Master->;
            
            if($id)
                return $this->apiResponse(['message'=>'Subject deleted','id'=>$id]);
            else
                return $this->apiResponse([]);;

            
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function updateSubject(Request $request)
    {
        try {
            $rules = [
                'school_id' => 'required|numeric',
                'subject_name' => 'required',
            ];
            $validatedData = Validator::make( $request->all(),$rules);
                if ($validatedData->fails()){          
                     return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }
           
            $objSubject_Master = new Subject_Master();
            $id = $objSubject_Master->inserUpdateData($request);
            return $this->apiResponse(['message'=>'Subject updated','id'=>$id]);

        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function addSubject(Request $request)
    {
        try {
            $rules = [
                    'school_id' => 'required|numeric',
                    'role'=>'required|in:student,teacher,admin,user,school',
                    'subject_name' => 'required',

            ];
            if($request->role !='super_admin')
            {
                $validatedData = Validator::make( $request->all(),$rules);
                if ($validatedData->fails()){          
                     return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
                }
            }
            $objSubject_Master = new Subject_Master();
            $id = $objSubject_Master->inserUpdateData($request);

            return $this->apiResponse(['message'=>'Subject added','id'=>$id]);

            
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function getSubjectList(Request $request)
    {
        try {
            $rules = [
                    'school_id' => 'required|numeric',
                    'role'=>'required|in:student,teacher,admin,user,school',
                    'class_section_id' => 'required'
            ];
            
            $validatedData = Validator::make( $request->all(),$rules);
           
            if ($validatedData->fails()){          
                 return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }
            $objSubject_Class = new Subject_Class();
            $data = $objSubject_Class->getSubject($request);
            if($data){
                if(!empty($request->page_limit)){
                    
                    return $this->apiResponse($data);
                }
                return $this->apiResponse(['data'=>$data]);
            }
            else
                return $this->apiResponse([]);;

            
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }
  

}
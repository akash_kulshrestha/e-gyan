<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use \Validator;
use Auth;
use App\Study_Material, App\Attachments_Table;
use \DB;


class StudyMaterialController extends \App\Http\Controllers\Controller
{
    public function deleteStudyMaterial(Request $request)
    {
        try {
            $rules = [
                'id' => 'required|numeric',
                'school_id' => 'required|numeric',
                'role' => 'required|in:student,teacher,admin,user,school'
            ];

            if ($request->role != 'super_admin') {
                $validatedData = Validator::make($request->all(), $rules);
                if ($validatedData->fails()) {
                    return $this->apiResponse(['error' => $validatedData->errors(), 'message' => $this->errorToMeassage($validatedData->errors())], true);
                }
            }

            $objStudy_Material = new  Study_Material();
            // $id= $objStudy_Material->;

            if ($id)
                return $this->apiResponse(['message' => 'Study material deleted', 'id' => $id]);
            else
                return $this->apiResponse([]);;
        } catch (\Exception $e) {
            return $this->apiResponse(['message' => 'Request not successful', 'error' => $e->getMessage()], true);
        }
    }

    public function addStudyMaterial(Request $request)
    {
        try {
            $rules = [
                'school_id' => 'required|numeric',
                'role' => 'required|in:student,teacher,admin,user,school',
                'title' => 'required|max:255'
            ];
            if (!empty($request->role) && $request->role == 'teacher') {
                $rules['teacher_class_subject_id'] = 'required';
            } else {
                $rules['class_section_id'] = 'required';
                $rules['subject_ids'] = 'required';
            }

            $validatedData = Validator::make($request->all(), $rules);

            if ($validatedData->fails()) {
                return $this->apiResponse(['error' => $validatedData->errors(), 'message' => $this->errorToMeassage($validatedData->errors())], true);
            }
            $objStudy_Material = new  Study_Material();
            $data = $objStudy_Material->addStudyMaterial($request);

            return $this->apiResponse(['message' => 'Study material added', "data" => $data]);
        } catch (\Exception $e) {
            return $this->apiResponse(['message' => 'Request not successful', 'error' => $e->getMessage()], true);
        }
    }

    public function getStudyMaterial(Request $request)
    {
        try {
            $rules = [
                'school_id' => 'required|numeric',
                'role' => 'required|in:student,teacher,admin,user,school'
            ];
            if (!empty($request->role) && $request->role == 'teacher') {
                $rules['teacher_class_subject_id'] = 'required';
            } else {
                $rules['class_section_id'] = 'required';
                //   $rules['subject_ids']='required';
            }
            $validatedData = Validator::make($request->all(), $rules);
            if ($validatedData->fails()) {
                return $this->apiResponse(['error' => $validatedData->errors(), 'message' => $this->errorToMeassage($validatedData->errors())], true);
            }
            $objStudy_Material = new  Study_Material();
            $data = $objStudy_Material->getStudyMaterial($request);

            if (!empty($request->page_limit)) {
                return $this->apiResponse($data);
            }
            return $this->apiResponse(['data' => $data]);
        } catch (\Exception $e) {
            return $this->apiResponse(['message' => 'Request not successful', 'error' => $e->getMessage()], true);
        }
    }

    public function getStudyMaterialByid($id,Request $request)
    {
        try {
            $rules = [
                'school_id' => 'required|numeric',
                'role' => 'required|in:student,teacher,admin,user,school'
            ];
            if (!empty($request->role) && $request->role == 'teacher') {
                $rules['teacher_class_subject_id'] = 'required';
            } else {
                $rules['class_section_id'] = 'required';
                //   $rules['subject_ids']='required';
            }
            $validatedData = Validator::make($request->all(), $rules);
            if ($validatedData->fails()) {
                return $this->apiResponse(['error' => $validatedData->errors(), 'message' => $this->errorToMeassage($validatedData->errors())], true);
            }
            $objStudy_Material = new  Study_Material();
            $data = $objStudy_Material->getStudyMaterial($request,$id);

            if (!empty($request->page_limit)) {
                return $this->apiResponse($data);
            }
            return $this->apiResponse(['data' => $data]);
        } catch (\Exception $e) {
            return $this->apiResponse(['message' => 'Request not successful', 'error' => $e->getMessage()], true);
        }
    }
}

<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use \Validator;
use Auth;
use App\App_Version;
use DB;


class AppVersionController extends \App\Http\Controllers\Controller
{
    public function getVersionListPage(Request $request){
         $response=DB::table('app_version')
        ->select('app_version.*','school.name as schoolName')
        ->leftjoin('school','school.id','=','app_version.school_id')
        ->get();
        return view('BackEnd/appVersionManagement.appVersion',compact("response"));
    }

    public function addUpdateAppVersion(Request $request)
    {
        try {
            $rules = [
                    'school_id' => 'required|numeric',
                    'role'=>'required|in:student,teacher,admin,user,school',
                    'app_version'=>'required',
                    'mandatory_status'=>'required|in:low,high,medium'
            ];
           
            $validatedData = Validator::make( $request->all(),$rules);
            if ($validatedData->fails()){          
             return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
            }

            $objApp_Version = new App_Version();
            $data= $objApp_Version->inserUpdateData($request);
            if(!empty($data['id'])){
                if(!empty($request->id)){
                    $data['message']='Successfully Updated';
                    return $this->apiResponse($data);
                }
                else
                {
                    $data['message']='Successfully Created';
                    return $this->apiResponse($data);                    
                }
            }
            else
                return $this->apiResponse(['error'=>$data, 'message'=>$data],true);            
        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }

    public function appVersionForm(Request $request){
     
        $schools = DB::table('school')->get();
        $status=['low','medium','high'];
        $appVersion=[];
    
        if(!empty($request->id)){
           $appVersion = (new App_Version())->find($request->id)->toArray();
        }
        $request=$request->all();
        return view('BackEnd/appVersionManagement.addeditform',compact("schools",'status','appVersion','request'));  
    }


	public function nextVersion(Request $request){
		try{
            $rules = [
                'school_id' => 'required|numeric',
                'app_version'=>'required',
                'role'=>'required|in:student,teacher,admin,user,school'
            ];
            if($request->role != 'super_admin')
            {
                $validatedData = Validator::make( $request->all(),$rules);
                if ($validatedData->fails()){          
                     return $this->apiResponse(['error' => $validatedData->errors() ,'message'=> $this->errorToMeassage($validatedData->errors()) ], true);
                }
            }
            $objApp_Version = new App_Version();
            $data = $objApp_Version->nextVersion($request);
            if($data)
                return $this->apiResponse(['data'=>$data]);
            else
                return $this->apiResponse(['message'=>'Your version is updated','data'=>true]);

        } catch(\Exception $e) {
            return $this->apiResponse(['message'=>'Request not successful','error'=>$e->getMessage()],true);
        }
    }
    

}

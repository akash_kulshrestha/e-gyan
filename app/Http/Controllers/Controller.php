<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
  
    public function apiResponse($data=[], $error = false,$code=200)
	{
		global $request;
		$defaultResponseArr = [];
		$responseData=[];
		if(!empty($data) && !isset($data['data']) ){
			$responseData['data']=$data;
		}else{
			$responseData=$data;
		}


		switch($error)
		{
			case true: 
			case '1':
			case 'true':
				$defaultResponseArr['success'] = false;				
				break;
			default:
				$defaultResponseArr['success'] = true;
		}
		if(empty($responseData['message']) && !empty($responseData['error'])){
			$responseData['message']=$responseData['error'];
		}
		if(!empty($request->header('device')) && $request->header('device')=="mobile"){
			unset($responseData['error']);
		}


		$defaultResponseArr['notification_count'] = DB::table('web_notification')->where(['user_role'=>$request->role,'user_id'=>$request->user_id,"status"=>0])->count();
		// return response()->json(array_merge($defaultResponseArr, $responseData), $code);
		return \Response::json( array_merge($defaultResponseArr, $responseData),$code );
	}


	public function errorToMeassage($error){
		$message=[];
		if(!empty($error)){
			$error= $error->toArray();
			foreach($error as $key => $value) {
				foreach ($value as  $val) {
					$message[]=$val;
				}
			}
		 }
		 return implode(",",$message);
	}

}

<?php

namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Teacher_Class_Subject, App\User;
use Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Tymon\JWTAuth\Payload;

class Teacher extends Authenticatable implements JWTSubject
{
    protected $table = 'teacher';
	
	protected $primaryKey = 'id';
	
	public $timestamps = false;

	public $fillable = ['id','school_id','name','isProfilePic','profile_pic_url','employee_id','date_of_joining','dob','email','gender','address','login_id','password','phone_no','pass_code','device_token','token','updated_at'];
	
	protected $hidden = [];
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

	public function teacher()
    {
        return $this->hasOne('App\Teacher_Class_Subject','teacher_id','id');
    }

    public function file_url()
    {
        return $this->hasOne('App\Attachments_Table','reference_id','id')->where('table_type',$this->table)->where('status',1);
    }

    public function school()
    {
        return $this->hasOne('App\School','id','school_id');
    }

    public function getTable(){
    	return $this->table;
    }

    public function getsignUpRules($request){

        $userType="";
        $userType = $request->role;

        $rules=['name'=> "required|string"];
        $rules['email'] = "required|email";
        $rules['password'] = 'required|min:3|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/';
        $rules['login_id'] = "required";
     
        $rules['employee_id'] = "required";
        $rules['school_id'] ="required|numeric";
        $rules['date_of_joining'] = "required|date";
        $rules['phone_no'] = "required|numeric|min:10";
        $rules['address'] = "required";;
         
      return $rules;
    }

     public function getTeacherList($request=null){
        $obj=self::with('file_url')->select('teacher.*',"school.name as school_name")
         ->leftjoin('school','school.id','=','teacher.school_id');
        if(session('user_school_id')!=''){
            $obj->where([ 'school_id'=>session('user_school_id')]);
        }
        return $obj->get();
    }

    public function getTeacherDetails($request=""){
        $user=[];
        if($request->id){
            $obj=self::with('file_url') ;
            $user= $obj->find($request->id);
            $user->password = (new User())->string_decrypt($user->pass_code);
        }
        return $user;
    }

      public function createUpdate($request){
        $insertData=[];
        if(!empty($request->id)){
            $id = $request->id;
            $insertData = self::find($id);
            $password=(new User())->string_decrypt($insertData->pass_code);
        }
        if(!empty($request["password"])){
            $request["pass_code"]= (new User())->string_encrypt($request["password"]);
            $request["password"]= Hash::make($request["password"]);
        }

        foreach ($this->fillable as $key => $value) {
          if(!empty($request[$value]))
            $insertData[$value]=$request[$value];
        }

        if(!empty($insertData)){
            if(!empty($insertData["id"])){
                self::where(['id'=>$insertData["id"]])->update($insertData->toArray());
            }else{
                $id = self::create($insertData )->id;
            }
            if($request->hasFile('files')){
               $objAttachments_Table= new Attachments_Table();
               $request->school_id=$id;
               $objAttachments_Table->UploadandSave($request, $id,$this->table,"Profile Image",true,"files");
            }
        }
        $teacherClassSection= $request->teacherClassSection?$request->teacherClassSection: $request['subject_class_id'];
        (new Teacher_Class_Subject())->addUpdateTeacherClassSubject($id, $request->school_id, $teacherClassSection); 

        return ["data"=>$id];   
    }

    


}

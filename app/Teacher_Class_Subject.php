<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Teacher;
use DB;

class Teacher_Class_Subject extends Model
{
    protected $table = 'teacher_class_subject';
	
	protected $primaryKey = 'id';
	
	public $timestamps = false;

	public $fillable = ['id','school_id','teacher_id','subject_class_id'];
	
	protected $hidden = [];	

	public function teacher()
    {
        return $this->belongsTo('App\Teacher','teacher_id','id');
    }
    
    public function file_url()
    {
        return $this->hasMany('App\Attachments_Table','reference_id','id')->where('table_type',$this->table)->where('status',1);
    }

    public function getTable(){
    	return $this->table;
    }
	
    public function getTeacherClassSubjectIds($class_section_id,$subject_ids, $school_id){
    		
    		$sub = DB::table('teacher_class_subject as t')->select(DB::raw("t.id"))->where(['t.status'=>1,'t.school_id'=>$school_id ])
			->Join('subject_class', 'subject_class.id', '=', 't.subject_class_id')
			->Join('subject_master', 'subject_master.id', '=', 'subject_class.subject_id')
			->Join('class_section', 'class_section.id', '=', 'subject_class.class_section_id');
			if(!empty($class_section_id)){
				$sub =	$sub->where('class_section.id',$class_section_id);
			}
	        if(!empty($subject_ids)){
	            if(!is_array($subject_ids))
					$subject_ids = [$subject_ids];
				$sub =	$sub->whereIn('subject_master.id',$subject_ids);
	         }       
			$sub = $sub->where("t.id", ">" ,0);
			$teacher_class_subject_id =	$sub->pluck('id')->toArray();
			return $teacher_class_subject_id;
    }


	public function checkClassSubject($teacher_id, $school_id, $teacherClassSection_ids){
		$data =self::where([
			'school_id'=>$school_id,
			'status'=>1
		]);
		if($teacherClassSection_ids){
			$data= $data->whereIn('subject_class_id',$teacherClassSection_ids);
		}
		if($teacher_id){
			$data=  $data->where('teacher_id',"<>",$teacher_id);
		}
		$data= $data->pluck('subject_class_id');
		return $data;
	}

    public  function addUpdateTeacherClassSubject($teacher_id, $school_id, $teacherClassSection_ids){
		$row= self::where(compact('teacher_id','school_id'))->update(['status'=>0]);
    	if(!empty($teacherClassSection_ids) && !empty($teacher_id)){
    		 foreach($teacherClassSection_ids as $teacherClassSub){
                $data=[
                    'school_id'=>$school_id,
                    'teacher_id'=>$teacher_id,
                    'subject_class_id'=>$teacherClassSub
                ];
                $row = self::where($data)->get()->toArray();
                if(empty($row)){
                     DB::table('teacher_class_subject')->insert($data);
                }else{
                	self::where(['id'=>$row[0]['id']])->update(['status'=>1]);
    		    }
            }
    	}
    }

	public  function getList($request){
	return 	$teacher_class_subject = Teacher_Class_Subject::select(
			'teacher_class_subject.id',
			'teacher_class_subject.subject_class_id   as subject_class_id',
			'subject_master.id as subject_id',
			'subject_master.subject_name',
			'class_section.id as class_section_id',
			'class_section.class_name',
			'class_section.section_name'
		  )
		->leftJoin('subject_class', 'subject_class.id', '=', 'teacher_class_subject.subject_class_id')
		->leftJoin('subject_master', 'subject_master.id', '=', 'subject_class.subject_id')
		->leftJoin('class_section', 'class_section.id', '=', 'subject_class.class_section_id')
		->where('teacher_class_subject.teacher_id', $request->user_id)
		->where('teacher_class_subject.status', 1)
		->get();
	}



}

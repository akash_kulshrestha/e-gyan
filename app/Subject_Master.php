<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject_Master extends Model
{
    protected $table = 'subject_master';
	
	protected $primaryKey = 'id';
	
	public $timestamps = false;

	public $fillable = ['id','subject_name',"school_id"];
	
	protected $hidden = [];	

	public function file_url()
    {
        return $this->hasMany('App\Attachments_Table','reference_id','id')->where('table_type',$this->table)->where('status',1);
    }

    public function getTable(){
    	return $this->table;
    }

    public function getList($request)
    {
         $data = self::select("subject_master.*","school.name as school_name") 
         ->leftjoin('school','school.id','=','subject_master.school_id');
        if($request->school_id > 0){
            $data->where("school_id",$request->school_id);
        }
        return $data->get();

    }
    public function inserUpdateData($request){
        
        if(!empty($request->id)){
            $id= $request->id;
            $insertData = self::find($id);
        }

        foreach ($this->fillable as $key => $value) {
            if(!empty($request[$value]))
                $insertData[$value]=$request[$value];
        }
        
       if(!empty($insertData)){
            if(!empty($insertData["id"])){
                self::where(['id'=>$insertData["id"]])->update($insertData->toArray());
            }else{
             $id = self::create($insertData)->id;
            }
        }
        return compact("id");
          
    }

}

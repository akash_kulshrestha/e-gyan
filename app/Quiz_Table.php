<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz_Table extends Model
{
    protected $table = 'quiz_table';
	
	protected $primaryKey = 'id';
	
	public $timestamps = false;

	public $fillable = ['id','class_section_id','subject_id','total_time','topic'];
	
	protected $hidden = [];	

    public function inserUpdateData($request){
        
            if(!empty($request->id)){
                $id= $request->id;
                $insertData = self::find($id);
            }

            foreach ($this->fillable as $key => $value) {
                if(!empty($request[$value]))
                    $insertData[$value]=$request[$value];
            }
            
           if(!empty($insertData)){
                if(!empty($insertData["id"])){
                    self::where(['id'=>$insertData["id"]])->update($insertData->toArray());
                }else{
                    // $row = self::where($insertData)->get()->toArray();
                    $row = self::where('subject_id',$insertData['subject_id'])->get()->toArray();
                    if(empty($row))
                        $id = self::create($insertData )->id;
                    else
                    {
                        //self::where(['id'=>$row[0]['id']])->update(['status'=>1]);
                        $id = $row[0]['id'];
                    }

                }
                //(new Attachments_Table())->insertAttechment_new($request, $id, $this->getTable());
                return compact("id");
            }
    }

    public function getTableData($request){
        if(!empty($request->class_section_id && $request->subject_id))
        {
            $data = self::where('class_section_id',$request->class_section_id)
                        ->where('subject_id',$request->subject_id)
                        ->get()->toArray();
            return $data;           
        }
    }

}

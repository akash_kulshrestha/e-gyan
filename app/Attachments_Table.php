<?php

namespace App;

// use FFI\Exception;
use Illuminate\Database\Eloquent\Model;
use Exception;

class Attachments_Table extends Model
{
    protected $table = 'attachments_table';
	protected $primaryKey = 'id';
	protected $hidden = [];	
	public $timestamps = false;

	public $fillable = ['id','school_id','file_label','file_url','reference_id','table_type','upload_data_time','filesize','file_type',"status"];

    public function getTable(){
    	return $this->table;
    }


    public function UploadandSave($request, $ref_id, $ref_table, $file_label,$single=false,$key="files"){
  		$destinationPath = public_path()."/uploads/$ref_table"; 
        $path = ['full_path'=>'http://'.$_SERVER['SERVER_NAME']."/uploads/$ref_table"];       
		foreach ($request->allFiles($key) as $file) {
	        foreach ($file as $fkey=> $fileobj) {
	        	
	        	$filedetails=[];
	            $Filename = date('YmdHis') .$fkey. "." . $fileobj->getClientOriginalExtension();
	            $filedetails['file_type']= $extension = $fileobj->getClientOriginalExtension();
              	$fileobj->move($destinationPath.'/'.$extension."/", $Filename);
                $filedetails['file_url']= "/uploads/".$ref_table.'/'.$extension."/".$Filename;
             	$filedetails['filesize']=30;
	            $this->savefile($request,$ref_id,$ref_table,$file_label,$single,$filedetails);
	        }
     	}
	}

	private function savefile($request,$ref_id,$ref_table,$file_label,$single=false,$file){
		$insertData =[
			'school_id'=>$request->school_id,
			'file_label'=>$file_label,
			'file_url'=>$file['file_url'],
			'reference_id'=>$ref_id,
			'table_type'=>$ref_table,
			'upload_data_time'=>date("Y-m-d H:i:s"),
			'filesize'=>$file['filesize'],
			'file_type'=>$file['file_type'],
			"status"=>1
			];
		
		$data=[];
      	if($single){
          	$data= self::where(
          			[
          				'reference_id'=>$insertData["reference_id"],
          				"table_type"=>$insertData["table_type"],
          				"file_label"=>$insertData["file_label"]
          			]
          		)->get();
		}
		if(!empty($data) && !empty($data[0]) && !empty($data[0]->id)){
          	self::where("id",$data[0]->id)->update($insertData);
          }
          else{
            self::create($insertData);
          }

	}


	public static function removeAttechment($id){
		self::where(compact('id'))->update(['status'=>0]);
	}




    public function addAttechments($request,$file_label,$reference_id,$table_type){
		$Role= ucwords (User::getRequestRole());
        $destinationPath = public_path()."/$Role"; // upload path
        $path = ['full_path'=>'http://'.$_SERVER['SERVER_NAME']."/$Role"];            

		 foreach ($request->allFiles($file_label) as $file) {

	        foreach ($file as $fkey=> $fileobj) {
				$file_url="";
	            $profileImage = date('YmdHis') .$fkey. "." . $fileobj->getClientOriginalExtension();
	            $extension = $fileobj->getClientOriginalExtension();
	            
	            if($extension == 'png' || $extension == 'gif' || $extension == 'jpg' || $extension == 'jpeg')
	            {
	                $fileobj->move($destinationPath.'/Image', $profileImage);
	                $file_url = $path['full_path'].'/Image/'.$profileImage;

	            }
	            else if($extension == 'doc' || $extension == 'docx')
	            {
	                $fileobj->move($destinationPath.'/Doc', $profileImage);
	                $file_url = $path['full_path'].'/Doc/'.$profileImage;
	            }
	            else if($extension == 'pdf')
	            {
	                $fileobj->move($destinationPath.'/Pdf', $profileImage);
	                $file_url = $path['full_path'].'/Pdf/'.$profileImage;
	            }
	        }
     	}
     	return $file_url;
    }


	public function insertAttechment($school_id, $file_url_list, $reference_id, $table_type,$file_type_value=null, $File_Label=null, $filesize=null ){
		if(!is_array($file_url_list)){
			$file_url_list=[$file_url_list];
		}
		foreach($file_url_list as $key => $file_url){	
			if(empty($file_type_value)){
				$filetype= explode(".",$file_url);
				$file_type=end($filetype);
			}else{
				$file_type=$file_type_value;
			}
			$upload_data_time=date('Y-m-d H:i:s');
			self::create(compact('school_id','file_label','file_url','reference_id','table_type','upload_data_time','filesize'))->id;			
		}
	}


	public function insertAttechment_new($request, $reference_id, $TableName){
		if(!empty($request->file_url)){
			self::where(['school_id'=>$request->school_id,'reference_id'=>$reference_id,'table_type'=>$TableName ])->update(['status'=>0]);
			$file_url_list= $request->file_url;
			if(!is_array($file_url_list)){
				$file_url_list=[$file_url_list];
			}
			foreach($file_url_list as $key => $file_url_obj){
				if(!empty($file_url_obj['url'])){
					$file_url   = $file_url_obj['url'];
					$school_id  = $request->school_id;
					$file_label = $file_url_obj['file_label'];
					$table_type = $TableName;
					if(empty($file_url_obj['filesize']))
						$filesize   =  30;
					else
						$filesize =  $file_url_obj['filesize'];
					if(empty($file_url_obj['file_type']))
					{
						$filetype  = explode(".",$file_url);
						$file_type =  end($filetype);
						$file_type  = $file_type;
					}
					else	
						$file_type = $file_url_obj['file_type'];

				}else{
					$filetype  = explode(".",$file_url_obj);
					$file_type =  end($filetype);
					$file_url   = $file_url_obj;
					$school_id  = $request->school_id;
					$file_label = "file_label";
					$table_type = $TableName;
					$filesize   =  30;
					$file_type  = $file_type;
				}
				
				$status   =1;
				$rowcondition =compact('school_id','file_url','reference_id','table_type');
				$rowdata=compact('school_id','file_label','file_url','reference_id','table_type','filesize','file_type','status');
				
				$row =self::where($rowcondition)->get()->toArray();
                if(empty($row)){
                     self::create($rowdata);	
                }else{
            		self::where('id',$row[0]['id'])->update($rowdata);
    		    }		
			}
		}
	}

	
	public function uploadAttechments($request,$Uploadtype=null){
		$file_url=[];
		$Role=$Uploadtype?$Uploadtype: ucwords (User::getRequestRole());
        $destinationPath = public_path()."/$Role"; // upload path
        $path = ['full_path'=>'http://'.$_SERVER['SERVER_NAME']."/$Role"];
		$allowedExten=['png','gif','jpg','jpeg','doc','docx','pdf','html'];

		foreach ($request->allFiles('files') as $file) {
			foreach ($file as $fkey=> $fileobj) {
	            $extension = $fileobj->getClientOriginalExtension();
				if(!in_array($extension,$allowedExten)){
					throw new \Exception($extension." type of file is not allowed please upload other files");
				}
			}
		}
		 foreach ($request->allFiles('files') as $file) {
			foreach ($file as $fkey=> $fileobj) {
	            $profileImage = date('YmdHis') .$fkey. "." . $fileobj->getClientOriginalExtension();
	            $extension = $fileobj->getClientOriginalExtension();
	            
	            if($extension == 'png' || $extension == 'gif' || $extension == 'jpg' || $extension == 'jpeg')
	            {
	                $fileobj->move($destinationPath.'/Image', $profileImage);
	                $file_url[] = $path['full_path'].'/Image/'.$profileImage;

	            }
	            else if($extension == 'doc' || $extension == 'docx')
	            {
	                $fileobj->move($destinationPath.'/Doc', $profileImage);
	                $file_url[] = $path['full_path'].'/Doc/'.$profileImage;
	            }
	            else if($extension == 'pdf')
	            {
	                $fileobj->move($destinationPath.'/Pdf', $profileImage);
	                $file_url[] = $path['full_path'].'/Pdf/'.$profileImage;
	            }
				else if($extension == 'html')
	            {
	                $fileobj->move($destinationPath.'/html', $profileImage);
	                $file_url[] = $path['full_path'].'/html/'.$profileImage;
	            }
	        }
     	}
     	return $file_url;
	}

	

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Teacher_Class_Subject, App\Teacher;
use App\Session_Table, App\Student, App\Attachments_Table;
use App\User, App\Notifaction, App\Assignment_Submittted,App\Assignment_Students;
use DB;
use Exception;

class Assignment extends Model
{
    protected $table = 'assignment';
	
	protected $primaryKey = 'id';
	
	public $timestamps = false;

	public $fillable = ['id','school_id','teacher_class_subject_id','title','assignment_description','due_date','created_date',
						'attachment_available','for_all'];
	
	protected $hidden = [];	

 	public function student_list()
    {
        return $this->hasMany('App\Assignment_Students','assignment_id','id');
    }

    public function file_url()
    {
        return $this->hasMany('App\Attachments_Table','reference_id','id')->where('table_type',$this->table)->where('status',1);
    }

    public function getTable(){
    	return $this->table;
    }
    
	public function teacher_class_subject()
    {
        return $this->belongsTo('App\Teacher_Class_Subject','teacher_class_subject_id','id')
        				->with(array('teacher'=>function($query){
                                        $query->select('id','name');
                                    }));
    }


    public function addUpdateAssignmentbyTeacher($request){
    
    		$insertDate = ['school_id'=>$request->school_id,
                            'teacher_class_subject_id'=>$request->teacher_class_subject_id,
                            'title'=>$request->title,
                            'assignment_description'=>$request->assignment_description,
                            'due_date'=>$request->due_date
                	];
			
            if(!empty($request->file_url)){
				$insertDate["attachment_available"]=1;
			 }
             $student_list= $request->student_list;
             if(empty($student_list)){
              $student_list =  $this->getAllStudentList(['school_id'=>$request->school_id,'teacher_class_subject_id'=>$request->teacher_class_subject_id]);
              $student_list= $student_list->toArray();
              $student_list=array_column($student_list,'student_id');
             }else{
                $insertDate['for_all']=0;
             }
            
             if(!empty($request->id)){
                $id =$request->id;
                $submitted= Assignment_Submittted::where(['assignment_id'=>$id])->first();
                if($submitted){
                    throw new Exception("This assignment is not editable because students start submission");
                }
                $record =self::where(['id'=>$id])->first();
                $start_date = new \DateTime($record->created_date);
                $since_start = $start_date->diff(new \DateTime());
                $minutes = $since_start->days * 24 * 60;
                $minutes += $since_start->h * 60;
                $minutes += $since_start->i;
                if($minutes > 120){
                    throw new Exception("This assignment is not editable because it passed 2 hours");
                }
                self::where(['id'=>$id])->update($insertDate )  ;
                (new Notifaction())->setNewNotificatin($request,$this->getTable(),$id,'update');
             }
             else{		 
                 $insertDate['created_date'] = date('Y-m-d H:i:s'); 
                 $id = self::create($insertDate )->id;
                 (new Notifaction())->setNewNotificatin($request,$this->getTable(),$id,'add');
             }
             Assignment_Students::where('status',0)->where(['assignment_id'=>$id])->delete();
             foreach($student_list as $student_id){

                Assignment_Students::create(['assignment_id'=>$id,'student_id'=>$student_id]);       
             }
		    (new Attachments_Table())->insertAttechment_new($request, $id, $this->getTable());
			return $id;
    }

    public function getAssignment($request,$id=null){

    	$objUser=new User();
	    $select='assignment.id as id ,teacher_class_subject_id ,title,assignment_description,due_date, teacher_class_subject.teacher_id ,for_all,assignment.created_date';
		$obj = self::select(DB::raw($select))->where([ 'assignment.school_id'=>$request->school_id]);

        if(!empty($request->teacher_class_subject_id)){
	  		if(!is_array($request->teacher_class_subject_id))
				$request->teacher_class_subject_id = [$request->teacher_class_subject_id];
			$obj->whereIn('teacher_class_subject_id',$request->teacher_class_subject_id);
        }else{
            return [];
        }
        // return $request->user_id;
        if($request->role=='student'){
            $assigment_ids=DB::table('assignment_students')->where('student_id',$request->user_id)->pluck('assignment_id');
            $obj->whereIn('assignment.id',$assigment_ids);
        }
        $obj->join('teacher_class_subject',["teacher_class_subject.id"=>"assignment.teacher_class_subject_id"]);
        $obj->join('teacher',["teacher.id"=>"teacher_class_subject.teacher_id"]);
        
        if(!empty($id)){
            $obj->where("assignment.id",$id);
        }
        $obj->with("file_url",'student_list');
		$obj->groupby('assignment.id');

        if(!empty($request->page_order)){
            $obj->orderby($request->page_order[0],$request->page_order[1]); 
        }
        if(!empty($request->page_limit)){
        	$data = $obj->paginate($request->page_limit)->toArray();
            foreach ($data['data'] as $key => $value) {
                $data['data'][$key]["teacher"]= $objUser->getUserDetails($value["teacher_id"] ,'teacher');
            }

        }else{
            $data = $obj->get()->toArray();
    		foreach ($data as $key => $value) {
    			$data[$key]["teacher"]= $objUser->getUserDetails($value["teacher_id"] ,'teacher');
    		}
        }
        if($request->role=='student'){
            foreach($data as $k=>$ass){
                $data[$k]['is_submitted']=false;
                $submitted= Assignment_Submittted::where(['student_id'=>$request->user_id,'assignment_id'=>$ass['id']])->with("file_url")->first();
                if($submitted){
                    $data[$k]['is_submitted']=true;
                    $data[$k]['subimtted_details']=$submitted; 
                }

            }
        }
        if(!empty($id)){
            return $data[0];
        }
        return  $data;

    }


     public function getMyCreatedAssingment($request){
    
        $obj= self::select('assignment.*','teacher.id as teacher_id')
                ->join('teacher_class_subject','teacher_class_subject.id','=','assignment.teacher_class_subject_id')
                ->join('teacher','teacher.id','=','teacher_class_subject.teacher_id')
                ->where(['assignment.school_id'=>$request->school_id]);

        if(!empty($request->teacher_class_subject_id)){
            if(!is_array($request->teacher_class_subject_id))
                $request->teacher_class_subject_id = [$request->teacher_class_subject_id];
            $obj->whereIn('assignment.teacher_class_subject_id',$request->teacher_class_subject_id)->with("file_url");
        }else{
            return [];
        }        
        $obj->with('student_list');
        if(!empty($request->page_order)){
            $obj->orderby($request->page_order[0],$request->page_order[1]); 
        }
        $objUser = new User();
        if(!empty($request->page_limit)){
            $data = $obj->paginate($request->page_limit)->toArray();
            foreach ($data['data'] as $key => $value) {
                $data['data'][$key]["teacher"]= $objUser->getUserDetails($value["teacher_id"] ,'teacher');
                if($request->role == 'student')
                    $data['data'][$key]["assignment_submitted_details"]= $this->getsubmittedAssigmentStudentDetailForCreatedAssingment($value,$request);
                $data['data'][$key]["all_students"]= count($this->getAllStudentList($value));
                $data['data'][$key]["submitted_students"]= count($this->getSubmitStudentList($value));
            }
        }else{
            $data = $obj->get()->toArray();
            foreach ($data as $key => $value) {
                $data[$key]["teacher"]= $objUser->getUserDetails($value["teacher_id"] ,'teacher');
                if($request->role == 'student')
                    $data[$key]["assignment_submitted_details"]= $this->getsubmittedAssigmentStudentDetailForCreatedAssingment($value,$request);
                $data[$key]["all_students"]= count($this->getAllStudentList($value));
                $data[$key]["submitted_students"]= count($this->getSubmitStudentList($value));
            }
        }
        return $data;
     }


     public function getAssingmentSubmittedStudentList($request){
        $obj= self::where(['school_id'=>$request->school_id,'teacher_class_subject_id'=>$request->teacher_class_subject_id])->with("file_url")->find($request->assignment_id);
        if(!empty($obj))
        {

            $data_student_submitted_assignment = $this->getSubmitStudentList($obj->toArray());
            // $submitted_assignment_student_id = [];
            // foreach ($data_student_submitted_assignment as $value) {
            //     $submitted_assignment_student_id[] = $value['student_id'];
            // }
            // $class_section_details = User::getclass_section($request);
            // $data_student_not_submitted_assignment = Student::where('class_section_id',$class_section_details->id) 
            //                                                 ->where('school_id',$class_section_details->school_id)
            //                                                 ->whereNotIn('id',$submitted_assignment_student_id)
            //                                                 ->get();
            // $result = array_merge($data_student_submitted_assignment->toArray(),$data_student_not_submitted_assignment->toArray());                                                
            // $data = $result;
            return $data_student_submitted_assignment->toArray();            
        }
        else
            return $obj;
     }   

     public function getsubmittedAssigmentStudentDetailForCreatedAssingment($data,$request){
         $obj= Assignment_Submittted::where("assignment_id",$data["id"])->where('student_id',$request->user_id)->with('student')->with('file_url');
         return $obj->first();
     }

     public function getsubmittedAssigmentStudentDetail($request){
         $obj= Assignment_Submittted::where("id",$request->assignment_submittted_id)->with('student')->with('file_url');
         return $obj->get();
     }

    function getAllStudentList($data){
        if(!empty($data["id"])){
            $student_id = DB::table('assignment_students')->where('assignment_id',$data["id"])->pluck('student_id');
        }
        $select="student.id as student_id,name,registration_no,roll_no,class_name,section_name";
        $obj = Student::select(DB::raw($select))->with('file_url')->where('student.school_id',$data["school_id"]);
        $obj->Join('class_section','class_section.id','=','student.class_section_id')
        ->Join('subject_class','subject_class.class_section_id','=','class_section.id')
        ->Join('teacher_class_subject','teacher_class_subject.subject_class_id','=','subject_class.id')
        ->where('teacher_class_subject.id',$data["teacher_class_subject_id"]);
        if(!empty($student_id) ){
            $obj=  $obj->whereIn('student.id',$student_id);
        }
        return $obj->get();
    }

    function getSubmitStudentList($data){
        $student_id = DB::table('assignment_students')->where('assignment_id',$data["id"])->where('status',1)->pluck('student_id');

        $select="student.id as student_id,name,registration_no,roll_no,class_name,section_name,submit_date,
        assignment_submittted.id as assignment_submittted_id,teacher_status,remark,grade";
        
        $obj =Student::select(DB::raw($select))->where('student.school_id',$data["school_id"])
        ->Join('assignment_submittted','assignment_submittted.student_id','=','student.id')
        ->Join('class_section','class_section.id','=','student.class_section_id')
        ->Join('subject_class','subject_class.class_section_id','=','class_section.id')
        ->Join('teacher_class_subject','teacher_class_subject.subject_class_id','=','subject_class.id')
        ->where('teacher_class_subject.id',$data["teacher_class_subject_id"])
        ->where("assignment_submittted.assignment_id",$data["id"])->whereIn('student.id',$student_id);
        return $obj->get();
    }
    

}

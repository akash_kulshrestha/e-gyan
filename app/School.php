<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\User;
use Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;

class School extends Authenticatable implements JWTSubject
{
	use Notifiable;
    protected $table = 'school';
	protected $quard = 'school';
	protected $primaryKey = 'id';
	
	public $timestamps = false;

	public $fillable = ['id','name','unique_sort_name','school_url','address','phone_no','email','password','login_id','pass_code','device_token','updated_at','token'];
	
	protected $hidden = [];
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
    
    public function getsignUpRules($request){

      $rules=['name'=> "required|string"];
      $rules['email'] = "required|email";
      $rules['password'] = 'required|min:3|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/';
      $rules['login_id'] = "required";
      $rules['phone_no'] = "required|numeric|min:10";
      return $rules;
    }

	public function file_url()
    {
        return $this->hasOne('App\Attachments_Table','reference_id','id')->where('table_type',$this->table)->where('status',1);
    }

    public function getTable(){
    	return $this->table;
    }

    public function getAlldata(){
    	return self::all();
    }

    public function getSchoolList($request=""){
        $obj=self::with('file_url') ;
        if(session('user_school_id')!=''){
            $obj->where([ 'id'=>session('user_school_id')]);
        }
        return $obj->get();
    }

     public function getSchoolinfo($user=[]){
        $obj=self::with('file_url');
        $obj=$obj->select(['id','name','unique_sort_name','school_url','address','phone_no','email'])->where([ 'id'=>$user['school_id']]);
        return $obj->first();
    }

    public function getAllDetails($id){
        $obj=self::with('file_url') ;
        $user= $obj->find($id);
        $user->password = (new User())->string_decrypt($user->pass_code);
        return $user;
    }

    public function createUpdate($request=[]){
        $insertData=[];
        if(!empty($request->id)){
            $id = $request->id;
            $insertData = self::find($id);
            $password=(new User())->string_decrypt($insertData->pass_code);
        }
        if(!empty($request["password"])){
            $request["pass_code"]= (new User())->string_encrypt($request["password"]);
            $request["password"]= Hash::make($request["password"]);
        }

        foreach ($this->fillable as $key => $value) {
          if(!empty($request[$value]))
            $insertData[$value]=$request[$value];
        }

        if(!empty($insertData)){
            if(!empty($insertData["id"])){
                self::where(['id'=>$insertData["id"]])->update($insertData->toArray());
            }else{
                $id = self::create($insertData )->id;
            }
            if($request->hasFile('files')){
               $objAttachments_Table= new Attachments_Table();
               $request->school_id=$id;
               $objAttachments_Table->UploadandSave($request, $id,$this->table,"School logo",true,"files");
            }
        }
        return ["data"=>$id];   
    }
    
}
<?php

namespace App;
use Hash,DB;
use Tymon\JWTAuth\Contracts\JWTSubject;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Student extends Authenticatable implements JWTSubject
{
    protected $table = 'student';
	
	protected $primaryKey = 'id';
	
	public $timestamps = false;

	public $fillable = ['id','school_id','name','registration_no','class_section_id','email','gender','address','login_id','password','contact_num',"father_name","mother_name",'roll_no','pass_code','device_token','dob','optional'];
	
	protected $hidden = [];
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
    public function getsignUpRules($request){

        $rules=['name'=> "required|string"];
        $rules['email'] = "required|email";
        $rules['password'] = 'required|min:3|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/';
        $rules['login_id'] = "required";
        $rules['school_id'] ="required|numeric";  
        $rules['registration_no']="required";
        $rules['roll_no']="required";
        $rules['class_section_id']="required|numeric";
        $rules['father_name']="required|string";
        $rules['mother_name']="required|string";
        $rules['contact_num']="required|numeric|min:10";
        $rules['dob'] = "required|date";
        $rules['address'] = "required";
        return $rules;
    }
	public function file_url()
    {
        return $this->hasOne('App\Attachments_Table','reference_id','id')->where('table_type',$this->table)->where('status',1);
    }

    public function class_section()
    {
        return $this->hasOne('App\Class_Section','id','class_section_id');
    }

    public function school()
    {
        return $this->hasOne('App\School','id','school_id');
    }

    public function getTable(){
    	return $this->table;
    }

    public function getStudentList($request=""){
        $obj=self::with('file_url','class_section') ;
        if(session('user_school_id')!=''){
            $obj->where([ 'school_id'=>session('user_school_id')]);
        }
        
        if(!empty($request->class_section_id)){
             $obj->where([ 'class_section_id'=>$request->class_section_id]);
        }
        return $obj->get();
    }

    public function getListPage($request){
         $students=self::select('student.*','class_section.class_name','class_section.section_name',"school.name as school_name")
         ->leftjoin('school','school.id','=','student.school_id')
        ->leftJoin('class_section', 'class_section.id', '=', 'student.class_section_id');

        if(session('user_school_id')!='')
            $students->where('student.school_id',session('user_school_id'));
        return $students->get();
    }

    public function getStudentDetails($request){
        $user=[];
        if($request->id){
            $obj=self::with('file_url') ;
            $user= $obj->find($request->id);
            $user->password = (new User())->string_decrypt($user->pass_code);
        }
        return $user;
    }

     public function createUpdate($request=[]){
        $insertData=[];
        if(!empty($request->id)){
            $id = $request->id;
            $insertData = self::find($id);
            $password=(new User())->string_decrypt($insertData->pass_code);
        }
        if(!empty($request["password"])){
            $request["pass_code"]= (new User())->string_encrypt($request["password"]);
            $request["password"]= Hash::make($request["password"]);
        }

        foreach ($this->fillable as $key => $value) {
          if(!empty($request[$value]))
            $insertData[$value]=$request[$value];
        }
        if(empty($insertData['device_token'])){
            $insertData['device_token']="empty comming soon";

        }
        if(!empty($insertData)){
            if(!empty($insertData["id"])){
                self::where(['id'=>$insertData["id"]])->update($insertData->toArray());
            }else{
                $id = self::create($insertData )->id;
            }
            if($request->hasFile('files')){
               $objAttachments_Table= new Attachments_Table();
               $request->school_id=$id;
               $objAttachments_Table->UploadandSave($request, $id,$this->table,"Profile Image",true,"files");
            }
        }
        return ["data"=>$id];   
    }

    function getAllStudentList($data){
        $select="student.id as id,name,registration_no,roll_no,class_name,section_name";
        $obj = Student::select(DB::raw($select))->with('file_url')->where('student.school_id',$data["school_id"]);
        $obj->Join('class_section','class_section.id','=','student.class_section_id')
        ->Join('subject_class','subject_class.class_section_id','=','class_section.id')
        ->Join('teacher_class_subject','teacher_class_subject.subject_class_id','=','subject_class.id')
        ->where('teacher_class_subject.id',$data["teacher_class_subject_id"]);
        return $obj->get();
    }

}

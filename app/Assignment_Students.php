<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;


class Assignment_Students extends Model
{
    protected $table = 'assignment_students';
	
	protected $primaryKey = 'id';
	
	public $timestamps = false;

	public $fillable = ['assignment_id','student_id','status'];
	protected $hidden = [];	

	public function student()
    {
        return $this->hasOne('App\Student','id','student_id');
    }
    public function assignment()
    {
        return $this->hasOne('App\Assignment','id','assignment_id');
    }

    public function getTable(){
    	return $this->table;
    }
}

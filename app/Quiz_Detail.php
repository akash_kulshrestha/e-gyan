<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz_Detail extends Model
{
    protected $table = 'quiz_detail';
	
	protected $primaryKey = 'id';
	
	public $timestamps = false;

	public $fillable = ['id','question_number','description','option_A','option_B','option_C','option_D','correct_answer','explaination','quiz_table_id'];
	
	protected $hidden = [];	

	public function inserUpdateData($request){
        
            if(!empty($request->id)){
                $id= $request->id;
                $insertData = self::find($id);
            }

            foreach ($this->fillable as $key => $value) {
                if(!empty($request[$value]))
                    $insertData[$value]=$request[$value];
            }
            
           if(!empty($insertData)){
                if(!empty($insertData["id"])){
                    self::where(['id'=>$insertData["id"]])->update($insertData->toArray());
                }else{
                    // $row = self::where($insertData)->get()->toArray();
                    $row = self::where('quiz_table_id',$insertData['quiz_table_id'])->get()->toArray();
                    if(empty($row))
                        $id = self::create($insertData )->id;
                    else
                    {
                        //self::where(['id'=>$row[0]['id']])->update(['status'=>1]);
                        $id = $row[0]['id'];
                    }

                }
                //(new Attachments_Table())->insertAttechment_new($request, $id, $this->getTable());
                return compact("id");
            }
    }

    public function getQuiz($request){
    	if(!empty($request->quiz_table_id))
    	{
    		$data = self::where('quiz_table_id',$request->quiz_table_id)->get()->toArray();
    		return $data;           
    	}
    }
}
